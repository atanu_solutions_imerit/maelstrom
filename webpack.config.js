const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const Dotenv = require('dotenv-webpack')

module.exports = env => {
	const devMode = process.env.NODE_ENV !== 'production';
	const dotenvpath = '.env';

	return {
		devtool: 'inline-source-map',
		// devtool: (devMode === 'development') ? 'inline-source-map' : false,
		performance: {
			maxEntrypointSize: 512000,
			maxAssetSize: 512000
		},
		mode: "development",
		optimization: {
			splitChunks: {
			cacheGroups: {
				styles: {
				name: 'styles',
				test: /\.(sa|sc|c)ss$/,
				chunks: 'all',
				enforce: true
				}
			}
			}
		},
		entry: {
			app :[ './src/scripts/app/app.tsx'],
			vendors: [
				"webpack-material-design-icons"
			]
		},
		output: {
			path : path.resolve(__dirname, 'dist'),
			filename : '[name]_bundle.[contenthash].js'
		},
		module : {
			rules : [
				{
					test: /\.tsx?$/,
					use : [
						{
							loader: 'ts-loader',
							options: {onlyCompileBundledFiles: true}
						}
					]
				},
				{
					test: /\.module.css$/,
					use: [
						MiniCssExtractPlugin.loader,
						{
							loader : 'typings-for-css-modules-loader',
							options : {
								url: false,
								modules: true,
								namedExport: true,
								camelCase: true,
								importLoaders: 1,
								localIdentName: '[name]__[local]__[hash:base64:5]'
							}
						},
						{
							loader : 'postcss-loader',
							options: {
								plugins: (loader) => [
									require('autoprefixer'),
								]
							}
						},
					]
				},
				{
					test: /\.(sa|sc|c)ss$/,
					exclude: [/\.module\.css$/],
					use: [
						//devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
						MiniCssExtractPlugin.loader,
						'css-loader',
						{
							loader : 'postcss-loader',
							options: {
								plugins: (loader) => [
								require('autoprefixer'),
								]
							}

						},
						'sass-loader',
					],
				},
				{
					test : /\.html$/,
					use : ['html-loader']
				},
				{
					test : /\.(png|jpeg|svg|woff|ttf|woff2|eot)$/,
					use : [
						{
							loader: 'file-loader',
							options : {
								name : '[name].[ext]',
								outputPath : 'img/',
								publicPath : 'img/'
							}
						}
					]
				}

			]
		},
		plugins: [
			new webpack.ProvidePlugin({
				$: 'jquery',
				jQuery: 'jquery',
				myOwnJSX: 'jsxFactory'
			}),
			new MiniCssExtractPlugin({
				filename: "bundle.[contenthash].css",
			}),
			new HtmlWebpackPlugin({
				template: 'src/index.html'
			}),
			new CleanWebpackPlugin(['dist']),
			new webpack.HashedModuleIdsPlugin(),
			new Dotenv({
				path: dotenvpath, // load this now instead of the ones in '.env'
				safe: true, // load '.env.example' to verify the '.env' variables are all set. Can also be a string to a different file.
				systemvars: true, // load all the predefined 'process.env' variables which will trump anything local per dotenv specs.
				silent: true, // hide any errors
				defaults: false // load '.env.defaults' as the default values if empty.
			})
		],
		resolve: {
			extensions: [ '.tsx', '.ts', '.js' ]
		},
	};
};
