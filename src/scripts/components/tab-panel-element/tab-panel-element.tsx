/**
 * @file This File houses UI Component  viz, TabPanelComponent which encompasses all Tab Panel Switching features
 */
import {myOwnJSX} from '../../utils/common/jsxFactory';
import MD5 from 'md5';
import tabStyles from './tabpanel.module.css';
import cx from 'classnames';

/**
 * @class
 * Implements Tabbed Panel UI Component
 */
export class TabPanelElement {
    /**
     * @private 
     * Keeps count of number of Tabs Required 
     */
    private child : number = 0;
    
    /**
     * Returns the Top Level UI Element of this component
     * @returns HTMLElement to be rendered by its parent container
     */
    public getView(): HTMLElement{
        return (
            <div style="display:block">
                <div >
                    <ul id={TabPanelElement.TABPANEL_ID.NAV_TABS} class={cx(tabStyles.mdTabs, 'nav', tabStyles.navTabs, 'nav-justified', 'blue', 'darken-4')}></ul> 
                    <div id={TabPanelElement.TABPANEL_ID.TAB_CONTENT_DIV} class="tab-content" style="margin-top: 20px;"></div>
                    <hr class={tabStyles.seperator}></hr>                    
                </div>
            </div>        

        )
    }

    /**
     * @public
     * Add New Panel Component to this View Component
     * @param tabName Name to be displayed in the Tab Pane
     * @param inputJSX JSX (HTMLElement) to be created as target for Tabs
     */
    public addPanelComponent(tabName: any, inputJSX:any, isTabNameJSX=false) {
        this.child ++;
        const hash = "l2_tab"+MD5(String(Math.random()));
        const tabNameHtml = (this.child === 1) ? 
            (<li id="test" class={cx(tabStyles.navItem, tabStyles.navitemFix)}>
                <a class={cx(tabStyles.navLink, 'active', 'show')} href={"#"+hash} aria-controls={hash} role="tab" data-toggle="tab">{tabName}</a>
            </li>) : 
            (<li class={cx(tabStyles.navItem, tabStyles.navitemFix)}>
                <a class={cx(tabStyles.navLink)} href={"#"+hash} aria-controls={hash} role="tab" data-toggle="tab">{tabName}</a>
            </li>);
        $('#'+TabPanelElement.TABPANEL_ID.NAV_TABS)!.append(tabNameHtml);

        let tabContentHtml = null
        this.child === 1 ?
            tabContentHtml = <div role="tabpanel" class="tab-pane active show" id={hash}> {inputJSX} </div> :
            tabContentHtml = <div role="tabpanel" class="tab-pane" id={hash}> {inputJSX} </div> 
        
        $('#'+TabPanelElement.TABPANEL_ID.TAB_CONTENT_DIV)!.append(tabContentHtml);
    }
}

export namespace TabPanelElement {
    /** Class or ID prefix used in this component */
    export enum TABPANEL_ID {
        TAB_CONTENT_DIV = '_tpe_content',
        NAV_TABS = "_tpe_nav_tabs"
    }
}