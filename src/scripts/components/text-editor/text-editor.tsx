/**
 * @file This File houses UI Component  viz, TabPanelComponent which encompasses all Tab Panel Switching features
 */
import {myOwnJSX} from '../../utils/common/jsxFactory';
import editorStyles from './texteditor.module.css';
import cx from 'classnames';
import { TranscribedTextSerializedData } from '../../services/messagebus/taskdata';
import { TSMap } from 'typescript-map';
import { TabPanelElement } from '../tab-panel-element/tab-panel-element';

/**
 * @class
 * Implements Tabbed Panel UI Component
 */
export class TextEditor {

    private _groupToTabMapping : TSMap<string, string> = new TSMap<string, string>();

    /**
     * @private
     * Handle for Generic View Components used by this View
     */
    private _tabPanelElement ?: TabPanelElement;

    /**
     * @constructor
     */
    constructor() {
        this._tabPanelElement = new TabPanelElement();
    }

    /**
     * Returns the Top Level UI Element of this component
     * @returns HTMLElement to be rendered by its parent container
     */
    public getView(): HTMLElement{
    return (
        <div class={cx("row", editorStyles.texteditor)}>
            <div class="container">
                <div class="row"><div class="col-md-12">&nbsp;</div></div>
                <div class="row">
                    <div class="col-md-8" id={TextEditor.ELEMENTS.TAB_CONTENT}>
                        <ul class="nav nav-tabs" id={TextEditor.ELEMENTS.TAB_SELECT}>
                        </ul>
                        <div class="tab-content tab-group">
                        </div>
                    </div>
                    <div class="col-md-4" style="padding-top: 45px;">
                        <table id="dtVerticalScrollExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="">Name</th>
                                    <th class="">Start Time</th>
                                    <th class="">End Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr><td>User 1</td><td><a href="javascript:void(0)">01:02:00</a></td><td><a href="javascript:void(0)">02:25:16</a></td></tr>
                                <tr><td>User 1</td><td><a href="javascript:void(0)">01:02:00</a></td><td><a href="javascript:void(0)">02:25:16</a></td></tr>
                                <tr><td>User 1</td><td><a href="javascript:void(0)">01:02:00</a></td><td><a href="javascript:void(0)">02:25:16</a></td></tr>
                                <tr><td>User 1</td><td><a href="javascript:void(0)">01:02:00</a></td><td><a href="javascript:void(0)">02:25:16</a></td></tr>
                                <tr><td>User 1</td><td><a href="javascript:void(0)">01:02:00</a></td><td><a href="javascript:void(0)">02:25:16</a></td></tr>
                                <tr><td>User 1</td><td><a href="javascript:void(0)">01:02:00</a></td><td><a href="javascript:void(0)">02:25:16</a></td></tr>
                                <tr><td>User 1</td><td><a href="javascript:void(0)">01:02:00</a></td><td><a href="javascript:void(0)">02:25:16</a></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>)
    }

    private createParagraph(contentId:string, contentStr : string) { // templatJSX: any,
        let p = document.createElement('p');
		p.innerHTML = contentStr;
        p.setAttribute('contenteditable', 'true');
        p.setAttribute('class', editorStyles.content);
		let content = document.getElementById(contentId);
		content!.appendChild(p);
    }

    private countWords(s:string){
        s = s.replace(/(^\s*)|(\s*$)/gi,"");//exclude  start and end white-space
        s = s.replace(/[ ]{2,}/gi," ");//2 or more space to 1
        s = s.replace(/\n /,"\n"); // exclude newline with a start spacing
        let word_count:number = s.split(' ').filter(function(str){return str!="";}).length;

        word_count = word_count + 1;
        return word_count;
    }

    private insertContentContainer(a: TranscribedTextSerializedData):string{

        let tabid = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 10);
        document.getElementById(TextEditor.ELEMENTS.TAB_CONTENT)!.appendChild(this._tabPanelElement!.getView());
        this._tabPanelElement!.addPanelComponent(a.gid, (
            <div class="panel-body">

                <div id={tabid} class="col-md-12" style="color: #000;min-height:200px;padding:0 20px;background-color: #fff;">
                    <div class="col-md-12 text-right">
                        <i class="make_bold material-icons">format_bold</i>
                        <i class="material-icons mx-1">format_italic</i>
                        <i class="material-icons mx-1">history</i>
                        <i class="material-icons mx-1">cloud_download</i>
                        <i class="material-icons mx-1">cloud_upload</i>
                        <span class="word-count mx-1" style="float:left"/>
                    </div>
                    <div class="contents"/>
                </div>
            </div>
        ));

        document.querySelector('.make_bold')!.addEventListener('click', (e:Event) => {
            this.makeBold(e);
            e.preventDefault();
        })

        return tabid;
    }

    private extractContent(doc:HTMLElement){

        let notes = "";
        let list = doc.getElementsByClassName("content");

        for (var i = 0; i < list.length; i++) {
            console.log(list[i].className);
            if (list[i].classList.contains("content")) {
              notes += list[i].innerHTML;
            }
        }

        return this.countWords(notes);
    }

    public updateContent(data: TranscribedTextSerializedData){

        let tabContentId = (this._groupToTabMapping.get(data.gid) === undefined ? this.insertContentContainer(data) : this._groupToTabMapping.get(data.gid));
        this._groupToTabMapping.set(data.gid, tabContentId);
        this.createParagraph(tabContentId, data.text); // (<p class="left content" contenteditable></p>)
        let content_tab = document.getElementById(tabContentId);
        let count_words = this.extractContent(content_tab!);
        $('#'+TextEditor.ELEMENTS.TAB_SELECT + " li").first().find("a").click(); // ('active');
    }

    public makeBold(event:Event){
        let highlight = window.getSelection();
        console.log(window.getSelection());
        let span = '<span style="font-weight:bold;">' + highlight + '</span>';
        let textHtml = ((event.target! as HTMLElement).parentNode!.parentNode! as HTMLElement).innerHTML;
        // console.log(span);
        // console.log(textHtml);
        ((event.target! as HTMLElement).parentNode!.parentNode! as HTMLElement).innerHTML = textHtml.replace(highlight.toString(), span);
    }
}

export namespace TextEditor {
    /** Class or ID prefix used in this component */
    export enum ELEMENTS {
        TAB_SELECT = '_tabs_select',
        TAB_CONTENT = '_tab_contents'
    }
}
