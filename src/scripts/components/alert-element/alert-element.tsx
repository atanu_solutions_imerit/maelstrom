/**
 * @file AlertManager UI Component to render system error, warning, error messages
 */
import cx from 'classnames';
import alertElementStyles from './alert-element.module.css';
import {myOwnJSX} from '../../utils/common/jsxFactory';
import '../../../img/cry.png';

export class AlertManager {

    static metaInfoTimer ?: NodeJS.Timer;
    /**
     * Function which manages rendering of metainfo display
     * @param msg Array of message subparts
     */
    static metaInfoDisplay(...msg : any[]) {
        $('#'+AlertManager.ALERT_ELEMENT.META_INFO_BLOCK).html(
            <div class={cx(alertElementStyles.notificationMessage)} >
            { Object.keys(msg[0]).map((a:string) => <span>{a} {msg[0][a]}</span>)}
            </div>
        );
        $('#'+AlertManager.ALERT_ELEMENT.META_INFO_BLOCK).css('display', 'block');

        if (this.metaInfoTimer !== undefined) {
            clearTimeout(this.metaInfoTimer)
            this.metaInfoTimer = undefined;
        }
        this.metaInfoTimer = setTimeout(() => {
            $('#'+AlertManager.ALERT_ELEMENT.META_INFO_BLOCK).empty();
            $('#'+AlertManager.ALERT_ELEMENT.META_INFO_BLOCK).css('display', 'none');
        }, 2000);
    }

    static getMetaInfoUiComponent() {
        return (
            <div>
                <div class={cx(alertElementStyles.notificationContainer)} id={AlertManager.ALERT_ELEMENT.META_INFO_BLOCK} style='display:none'>
                    <div class={cx(alertElementStyles.notificationMessage)} >
                    </div>
                </div>
                <div id={AlertManager.ALERT_ELEMENT.IMAGE_ERROR} style='display:none'>
                    <div class={cx(alertElementStyles.popLayer)}></div>
                    <div class={cx(alertElementStyles.popContext)}>
                        <div class={cx(alertElementStyles.popBox)}>
                            <img class="imgClass" src="img/cry.png" style="max-width: 15%;" />
                        </div>
                        <div class="pop-text-box">
                            <span class={cx(alertElementStyles.txtOne)}>
                                Either the image could not be downloaded or is corrupted.
                            </span>
                            <span class={cx(alertElementStyles.txtTwo)}>
                                Reload to see if it persists
                            </span>
                            <div class={cx(alertElementStyles.btnHolderAlert)}>
                                <button class="btn-holder">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }


    /**
     * Interface to call for handling all messages and user confirmation
     * @param type AlertManager.NOTIFICATION_TYPE
     * @param msg Message to be displayed
     */
    static processMsg(type: AlertManager.NOTIFICATION_TYPE, ...msg:any[]) : Promise<any> {
        let randId=Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 10);
        if (type == AlertManager.NOTIFICATION_TYPE.META_INFO_DISPLAY) {
            this.metaInfoDisplay(msg[0])
            return Promise.resolve();
        }

        if (type !== AlertManager.NOTIFICATION_TYPE.USER_INPUT_NOTIFICATION) {
            $('body').append(
                <div class={cx(alertElementStyles.notificationContainer)} id={randId}>
                    <div class={cx(alertElementStyles.notificationMessage)}>
                        <span>{msg}</span>
                    </div>
                </div>
            )
            setTimeout(() => {
                $('#'+randId).remove();
            }, 5000);
        }
        return this.insertLog(type, msg[0])
    }

    static processPopUpAlerts() {
        $('#'+AlertManager.ALERT_ELEMENT.IMAGE_ERROR).show();
        $('#'+AlertManager.ALERT_ELEMENT.IMAGE_ERROR).find('button').one('click', () => {
            $('#'+AlertManager.ALERT_ELEMENT.IMAGE_ERROR).hide()
        });
    }

    /**
     * Renders Notification tabpanel elements
     */
    static getUiComponents() {
        return (
                <div id={AlertManager.ALERT_ELEMENT.NOTIFICATION_HOLDER} class={cx(alertElementStyles.notificationHolder)}/>
        );
    }

    /**
     * Message string gets displayed and inserted in logs
     * @param type AlertManager.NOTIFICATION_TYPE
     * @param message Message to be displayed
     */
    static insertLog(type : AlertManager.NOTIFICATION_TYPE, message : string) : Promise<any> {
        let notificationHolder = document.getElementById(AlertManager.ALERT_ELEMENT.NOTIFICATION_HOLDER)
        switch(type) {
            case AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION:
                if (notificationHolder) {
                    notificationHolder.appendChild(
                        (<div>
                            <div class={cx(alertElementStyles.notificationImgHolder)}>
                                <i class="material-icons">error</i>
                            </div>
                            <div class={cx(alertElementStyles.notificationLabelHolder)}>
                                {message}
                            </div>
                        </div>)
                    )
                }
                return Promise.resolve();
            case AlertManager.NOTIFICATION_TYPE.INFO_NOTIFICATION:
                //let notificationHolder = document.getElementById(AlertManager.ALERT_ELEMENT.NOTIFICATION_HOLDER)
                if (notificationHolder) {
                    notificationHolder.appendChild(
                        (<div>
                            <div class={cx(alertElementStyles.notificationImgHolder)}>
                                <i class="material-icons">info</i>
                            </div>
                            <div class={cx(alertElementStyles.notificationLabelHolder)}>
                                {message}
                            </div>
                        </div>)
                    )
                }
                return Promise.resolve();
            case AlertManager.NOTIFICATION_TYPE.WARNING_NOTIFICATION:
                if (notificationHolder) {
                    notificationHolder.appendChild(
                        (<div>
                            <div class={cx(alertElementStyles.notificationImgHolder)}>
                                <i class="material-icons" style="color:#c5c525">warning</i>
                            </div>
                            <div class={cx(alertElementStyles.notificationLabelHolder)}>
                                {message}
                            </div>
                        </div>)
                    )
                }
                return Promise.resolve();
            default:
                return Promise.reject('Invalid Type found for Notification');
        }

    }

}

export namespace AlertManager {
    /** Type of Notitification Required */
    export enum NOTIFICATION_TYPE {
        USER_INPUT_NOTIFICATION,
        ERROR_NOTIFICATION,
        WARNING_NOTIFICATION,
        INFO_NOTIFICATION,
        META_INFO_DISPLAY
    }

    export enum ALERT_ELEMENT {
        NOTIFICATION_HOLDER = 'notificationholder',
        META_INFO_BLOCK = 'metaInfoBlock',
        IMAGE_ERROR = 'imageError'
    }
}
