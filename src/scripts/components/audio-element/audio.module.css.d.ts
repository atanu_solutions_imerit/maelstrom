export const playerContainer: string;
export const progressBarContainer: string;
export const tooltip: string;
export const volumeControl: string;
export const rngVolume: string;
export const playbackRange: string;
export const playbackRangeContainer: string;
