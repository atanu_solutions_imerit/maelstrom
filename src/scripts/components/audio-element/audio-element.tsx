/**
 * @file This File houses UI Component  viz, Audio Element component to responsible for handle audio element properties.
 */
import {myOwnJSX} from '../../utils/common/jsxFactory';
import playerStyles, { playbackRange }  from './audio.module.css';
import cx from 'classnames';
import { fromEventPattern, merge } from 'rxjs';

/**
 * @class
 * Implements Audio Elements UI Component
 */
export class AudioElement {
    /**
     * @private
     * Audio URL
     */
    private _clipUrl : string = '';

    /**
     * Returns the Top Level UI Element of this component
     * @returns HTMLElement to be rendered by its parent container
     */
    public getView(): HTMLElement{
        return (

            <div class="row">
                <div class="container">
                    <div class="row">
                        <audio id={AudioElement.ELEMENTS.AUDIO_ELEM}></audio>
                        <div  class={cx("container", playerStyles.playerContainer)}>
                            <div class={cx("col-sm-12", playerStyles.progressBarContainer)} >
                                <div>
                                        {/*<progress id={AudioElement.ELEMENTS.PROGRESS} value="0" max="1">&nbsp; </progress>*/}
                                        <progress id={AudioElement.ELEMENTS.SEEK_OBJ} value="0" max="1">&nbsp;</progress>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <i class="material-icons" id={AudioElement.ELEMENTS.AUDIO_REWIND}>fast_rewind</i>
                                        <i class="material-icons" id={AudioElement.ELEMENTS.AUDIO_PLAY}>play_circle_outline</i>
                                        <i class="material-icons" id={AudioElement.ELEMENTS.AUDIO_FORWARD}>fast_forward</i>
                                        <i class="material-icons" id={AudioElement.ELEMENTS.AUDIO_RESTART}>settings_backup_restore</i>
                                    </div>
                                    <div class="col-sm-2">
                                        <div id={AudioElement.ELEMENTS.VOLUME_CONTROL} class={cx(playerStyles.volumeControl)}>
                                            <i class="material-icons" id={AudioElement.ELEMENTS.AUDIO_VOLUME}> volume_up</i>
                                            <input type="range"  id={AudioElement.ELEMENTS.RNG_VOLUME}
                                                class={cx(playerStyles.rngVolume)} min={AudioElement.VOLUME_MIN}
                                                max={AudioElement.VOLUME_MAX} step={AudioElement.VOLUME_STEP} value={AudioElement.VOLUME_DEF}/>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <span id={AudioElement.ELEMENTS.AUDIO_CURTIME}>0.00/</span>
                                        <span id={AudioElement.ELEMENTS.AUDIO_DURATION}>0.00</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class={cx(playerStyles.playbackRangeContainer)}>
                                            <span id={AudioElement.ELEMENTS.PLAYBACKSPEED}>1.0x</span>
                                            <input type="range"  id={AudioElement.ELEMENTS.PLAYBACKRANGE}
                                                class={cx(playerStyles.playbackRange)} min={AudioElement.PLAYBACK_MIN_SPEED}
                                                max={AudioElement.PLAYBACK_MAX_SPEED} step={AudioElement.PLAYBACK_SPEED_STEP} value={AudioElement.PLAYBACK_DEF_SPEED}></input>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }

    /**
     * @private
     * Play/Pause toggler for AV content
     */
    private playClip() {
        try {
            const playBtn = document.getElementById(AudioElement.ELEMENTS.AUDIO_PLAY) as HTMLButtonElement;
            const oAudio = (document.getElementById(AudioElement.ELEMENTS.AUDIO_ELEM) as HTMLAudioElement);

            if (oAudio.paused) {
                oAudio.play();
                playBtn.textContent = "pause_circle_outline";
            }
            else{
                oAudio.pause();
                playBtn.textContent = "play_circle_outline";

            }
        }
        catch (e) {
            //console.log(e);
        }

    }

    /**
     * @private
     * Rewind playback by configurable interval
     */
    private rewindClip(){
        const oAudio = (document.getElementById(AudioElement.ELEMENTS.AUDIO_ELEM) as HTMLAudioElement);
        let pos = oAudio.currentTime - AudioElement.RW_FF_JUMP_INTERVAL;
        oAudio.currentTime = Math.max(0.0,pos);
    }

    /**
     * @private
     * Fast forward playback by configurable interval
     */
    private forwardClip(){
        const oAudio = (document.getElementById(AudioElement.ELEMENTS.AUDIO_ELEM) as HTMLAudioElement);
        let pos = oAudio.currentTime + AudioElement.RW_FF_JUMP_INTERVAL;
        oAudio.currentTime = Math.min(oAudio.duration,pos);
    }

    /**
     * @private
     * Restart playback by button click
     */
    private reStartClip(){
        const oAudio = (document.getElementById(AudioElement.ELEMENTS.AUDIO_ELEM) as HTMLAudioElement);
        oAudio.currentTime = 0.0;
    }

    /**
     * @private
     * update playback time with audio play & pause
     */
    private updateTime(){
        const oAudio = (document.getElementById(AudioElement.ELEMENTS.AUDIO_ELEM) as HTMLAudioElement);
        const playbtn = document.getElementById(AudioElement.ELEMENTS.AUDIO_PLAY) as HTMLButtonElement;

        const curLength:number  = oAudio.currentTime;
        const curTime:string = this.calculateCurrentDuration(curLength);
        const curTimeElem = document.getElementById(AudioElement.ELEMENTS.AUDIO_CURTIME) as HTMLElement;

        curTimeElem.innerHTML = curTime;
        //MessageBus.messageBusSubject.next(new Activity(Operation.PLAYBACK_POSITION,undefined,null,curLength));
        if(oAudio.ended){
            playbtn.textContent = "play_circle_outline";
        }
    }

    /**
     * @private
     * calculate and return playback total duaration as mm:ss format
     */
    private  calculateTotalduration(duration:number) {
        const minutes:number = Math.floor(duration/60);
        const seconds:string = (Math.floor(duration%60).toString()as any).padStart(2, '0');
        const time:string = minutes + ':' + seconds;

        return time;
    }

    /**
     * @private
     * calculate and return playback current duaration as mm:ss format
     */
    private calculateCurrentDuration(curDuration:number) {

        const curMinutes:number = Math.floor(curDuration/60);
        const curSeconds:string = (Math.floor(curDuration%60).toString()as any).padStart(2, '0');
        const curTime:string = curMinutes+":"+curSeconds+"/";

        return curTime;
    }

    /**
     * @private
     * calculate and progress of the playback
     */
    private initProgressBar(){
        const oAudio = (document.getElementById(AudioElement.ELEMENTS.AUDIO_ELEM) as HTMLAudioElement);
        const progressBar:any = document.getElementById(AudioElement.ELEMENTS.SEEK_OBJ);

        progressBar.value = (oAudio.currentTime / oAudio.duration);
        progressBar.addEventListener("click", (event:any) =>{
            var percent = event.offsetX / progressBar.offsetWidth;
            oAudio.currentTime = percent * oAudio.duration;
            progressBar.value = percent / 100;
        });
    }

    /**
     * @private
     * Handle the toggle of volume button on action
     */
    private volumeBtnToggle(){
        const volumeBtn = (document.getElementById(AudioElement.ELEMENTS.AUDIO_VOLUME) as HTMLButtonElement);
        const buttonContent:string|null = volumeBtn.textContent;
        const oAudio = (document.getElementById(AudioElement.ELEMENTS.AUDIO_ELEM) as HTMLAudioElement);

        if(buttonContent == 'volume_off'){
            volumeBtn.textContent = 'volume_up';
            oAudio.muted = false;
        }
        else{
            volumeBtn.textContent = 'volume_off';
            oAudio.muted = true;
        }

    }

    /*
     *   To handle different audio element's changed properties
    */
    public start(callback:Function) {
         /**
         * Define audio elements and html elements.
         */
        const oAudio = (document.getElementById(AudioElement.ELEMENTS.AUDIO_ELEM) as HTMLAudioElement);
        const rangeVolume = document.getElementById(AudioElement.ELEMENTS.RNG_VOLUME) as HTMLInputElement;
        const playbackRange = document.getElementById(AudioElement.ELEMENTS.PLAYBACKRANGE) as HTMLInputElement;
        const playbackSpeed = document.getElementById(AudioElement.ELEMENTS.PLAYBACKSPEED) as HTMLInputElement;
        oAudio.playbackRate = 1;

        /**
         * Load MetaData of audio element
         * To show the playback duration of the audio element
         */
        const loadedmetadata = fromEventPattern((handler:any) => {
            oAudio.addEventListener('loadedmetadata', handler);
        }, (handler:any) => {
            oAudio.removeEventListener('loadedmetadata', handler);
        });
        loadedmetadata.subscribe(() => {
            const time:string = this.calculateTotalduration(oAudio.duration);
            (document.getElementById(AudioElement.ELEMENTS.AUDIO_DURATION) as HTMLElement).innerHTML = time;
        });

        /**
         * Handle the properties changes of audio playback time
         * Time Update callback from HTML5 Audio Element
         */
        const timeupdate = fromEventPattern( (handler:any) =>
        {
            oAudio.addEventListener('timeupdate', handler);
        }, (handler:any) =>
        {
            oAudio.removeEventListener('timeupdate', handler);
        });
        timeupdate.subscribe(() => {
            this.updateTime();
            this.initProgressBar();
            callback(AudioElement.EVENT.PLAYBACK_POSITION,oAudio.currentTime);
        });

        /**
         * Handle the properties changes of volume
         * Volume Update callback to send data to view state.
         */
        const volumeControlObs1 = fromEventPattern( (handler:any) =>
        {
            rangeVolume.addEventListener('input', handler);
        }, (handler:any) =>
        {
            rangeVolume.removeEventListener('input', handler);
        });

        const volumeControlObs2 = fromEventPattern( (handler:any) =>
        {
            rangeVolume.addEventListener('change', handler);
        }, (handler:any) =>
        {
            rangeVolume.removeEventListener('change', handler);
        });

        merge(volumeControlObs1, volumeControlObs2).subscribe((x:any) => {
            let oAudio = document.getElementById(AudioElement.ELEMENTS.AUDIO_ELEM) as HTMLAudioElement ;
            let volumeBtn = document.getElementById(AudioElement.ELEMENTS.AUDIO_VOLUME) as HTMLButtonElement ;
            oAudio.volume = parseFloat(rangeVolume.value);

            callback(AudioElement.EVENT.VOLUME,oAudio.volume);
            if(oAudio.volume > 0.0){
                oAudio.muted = false;
                volumeBtn.textContent = 'volume_up';
            } else {
                oAudio.volume = 0;
                oAudio.muted = true;
                volumeBtn.textContent = 'volume_off';

            }
        });



        /**
         * Handle the properties changes of playback speed
         * Playback Speed Update callback to send data to view state.
         */

        const playbackControlObs1 = fromEventPattern( (handler:any) =>
        {
            playbackRange.addEventListener('input', handler);
        }, (handler:any) =>
        {
            playbackRange.removeEventListener('input', handler);
        });

        const playbackControlObs2 = fromEventPattern( (handler:any) =>
        {
            playbackRange.addEventListener('change', handler);
        }, (handler:any) =>
        {
            playbackRange.removeEventListener('change', handler);
        });

        merge(playbackControlObs1, playbackControlObs2).subscribe((x:any) => {
            oAudio.playbackRate = parseFloat(playbackRange.value);
            callback(AudioElement.EVENT.PLAYBACK_SPEED,oAudio.playbackRate);
            playbackSpeed.textContent = parseFloat(playbackRange.value).toFixed(1)+"x ";
        });


        const startstopTime = fromEventPattern( (handler:any) =>
        {
            document.addEventListener('keydown', handler);
        }, (handler:any) =>
        {
            document.removeEventListener('keydown', handler);
        });
        //console.log(startstopTime);
        startstopTime.subscribe((e:any) => {
            const playBtn = document.getElementById(AudioElement.ELEMENTS.AUDIO_PLAY) as HTMLButtonElement;
            const oAudio = (document.getElementById(AudioElement.ELEMENTS.AUDIO_ELEM) as HTMLAudioElement);
            if(e.keyCode == 13){
                oAudio.play();
                callback(AudioElement.EVENT.TIMELINEDATASTART,oAudio.currentTime);
                playBtn.textContent = "pause_circle_outline";
            }else if(e.keyCode == 27){
                oAudio.pause();
                callback(AudioElement.EVENT.TIMELINEDATAEND,oAudio.currentTime);
                playBtn.textContent = "play_circle_outline";
            }
        });

        // action to play clip
        (document.getElementById(AudioElement.ELEMENTS.AUDIO_PLAY) as HTMLButtonElement).addEventListener("click",() => {
            this.playClip();
        });

        // action to rewind clip
        (document.getElementById(AudioElement.ELEMENTS.AUDIO_REWIND) as HTMLButtonElement).addEventListener("click",() => {
            this.rewindClip();
        });

        // action to forward clip
        (document.getElementById(AudioElement.ELEMENTS.AUDIO_FORWARD) as HTMLButtonElement).addEventListener("click",() => {
            this.forwardClip();
        });

        // action to restart clip
        (document.getElementById(AudioElement.ELEMENTS.AUDIO_RESTART) as HTMLButtonElement).addEventListener("click",() => {
            this.reStartClip();
        });

        // action to handle volume button toggle
        (document.getElementById(AudioElement.ELEMENTS.AUDIO_VOLUME) as HTMLButtonElement).addEventListener("click",() => {
            this.volumeBtnToggle();
        });
    }

    /**
     * Updating clip url
     * @param link URL string
     */
    public updateClip(link: string) {
        this._clipUrl = link;
        const oAudio = (document.getElementById(AudioElement.ELEMENTS.AUDIO_ELEM) as HTMLAudioElement);
        const rangeVolume = document.getElementById(AudioElement.ELEMENTS.RNG_VOLUME) as HTMLInputElement;

        oAudio.src = this._clipUrl;
    }
}

export namespace AudioElement {
    /** Class or ID prefix used in this component */
    export enum ELEMENTS {
        AUDIO_ELEM = '_audio_src',
        AUDIO_PLAY = '_audio_play',
        AUDIO_REWIND = '_audio_rewind',
        AUDIO_FORWARD = '_audio_forward',
        AUDIO_RESTART = '_audio_restart',
        AUDIO_DURATION = '_audio_duration',
        AUDIO_CURTIME = '_audio_curtime',
        AUDIO_VOLUME = '_audio_volume',
        SEEK_OBJ = '_seek_obj',
        RNG_VOLUME = '_rng_volume',
        VOLUME_CONTROL = '_volume_control',
        PROGRESS = '_progress',
        PLAYBACKRANGE = '_playbackrange',
        PLAYBACKSPEED = '_playbackspeed',

    }

    /** defined values used in this component */
    export const RW_FF_JUMP_INTERVAL = 10.0;
    export const PLAYBACK_MIN_SPEED = 0.5;
    export const PLAYBACK_MAX_SPEED = 5.0;
    export const PLAYBACK_SPEED_STEP = 0.5;
    export const PLAYBACK_DEF_SPEED = 1.0;

    export const VOLUME_MIN = 0.0;
    export const VOLUME_MAX = 1.0;
    export const VOLUME_STEP = 0.01;
    export const VOLUME_DEF = 0.5;

    /** list of values used for different evets */

    export enum EVENT {
        PLAYBACK_POSITION,
        PLAYBACK_SPEED,
        VOLUME,
        TIMELINEDATASTART,
        TIMELINEDATAEND,
    }
}

