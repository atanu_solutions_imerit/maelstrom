
/**
 * @file LoaderManager UI Component to render system loading
 */
import cx from 'classnames';
import loaderElementStyles from './loader-element.module.css';
import {myOwnJSX} from '../../utils/common/jsxFactory';

export class LoaderManager {
    /**
     * Interface to call for handling all messages and user confirmation
     * @param type AlertManager.NOTIFICATION_TYPE
     * @param msg Message to be displayed
     */
    static processLoading(type: LoaderManager.LOADING_TYPE, sizelist:any, ...msg:any[]) {
        if(type == LoaderManager.LOADING_TYPE.USER_SUBMIT_NOTIFICATION) {
            if(msg[0] === 'show') {
                LoaderManager.getUiComponents(sizelist);
            }
            else if(msg[0] === 'hide') {
                LoaderManager.hideUiComponents();
            }
            else {
                LoaderManager.getUiComponents(sizelist);
            }
        }
        else {
            if(msg[0] === 'show') {
                LoaderManager.getUiSemiLoaderComponent();
            }
            else if(msg[0] === 'hide') {
                LoaderManager.hideUiSemiComponent();
            }
        }
    }

    /**
     * Renders Notification tabpanel elements
     */
    static getUiComponents(sizelistarg:any) {
        return (
            $('body').append(
                <div id="loading_area" class={cx(loaderElementStyles.loaderHolder)} style="display: block;">
                    <div class={cx(loaderElementStyles.loaderLines)}>
                        <div class={cx(loaderElementStyles.line01)}></div>
                        <div class={cx(loaderElementStyles.line02)}></div>
                        <div class={cx(loaderElementStyles.line03)}></div>
                    </div>
                    <div class={cx(loaderElementStyles.loaderTextLines)}>
                        <div class={cx(loaderElementStyles.loaderText)}>Please wait, while we submit..</div>
                    </div>
                </div>   
            )
        );
    }

    static getUiSemiLoaderComponent() {
        return (
            $('#play').append(
                <div id="loading_area_semi" class={cx(loaderElementStyles.loaderHolder)} style="display: block;">
                    <div class={cx(loaderElementStyles.loaderLines)}>
                        <div class={cx(loaderElementStyles.line01)}></div>
                        <div class={cx(loaderElementStyles.line02)}></div>
                        <div class={cx(loaderElementStyles.line03)}></div>
                    </div>
                    <div class={cx(loaderElementStyles.loaderTextLines)}>
                        <div class={cx(loaderElementStyles.loaderText)}>Setting Up..Please wait</div>
                    </div>
                </div>   
            )
        );
    }

    static hideUiComponents() {
        return (
            $('#loading_area').remove()
        );
    }

    static hideUiSemiComponent() {
        return (
            $('#loading_area_semi').remove()
        )
    }
}

export namespace LoaderManager {
    /** Type of Notitification Required */
    export enum LOADING_TYPE {
        USER_SUBMIT_NOTIFICATION,
        PLAIN_NOTIFICATION
    }
}  
