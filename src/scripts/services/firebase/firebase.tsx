import * as firebase from "firebase/app";
import "firebase/firestore";
import config from "./config";

const app: firebase.app.App= firebase.initializeApp(config);
const firestore = firebase.firestore(app);
const settings = {timestampsInSnapshots: true};
firestore.settings(settings);
export const db = firestore;
export default firebase;