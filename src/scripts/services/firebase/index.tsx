import { db } from "./firebase";
import { AlertManager } from "../../components/alert-element/alert-element";
//import { AnnotationStateSerializedData } from "../messagebus/taskdata";
/**
 * Handles all firebase data transactions
 */
export default class Firebase {
    /**
     * Extracts query string value from url
     * @param {string} sParam 
     * @returns {string} url query value
     */
    static getUrlParameter(sParam: string): string {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split("&"),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split("=");
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? "" : sParameterName[1];
            }
        }
        return "";
    }

    private nodeTaskId: string;
    private userCode: string;
    private port: string;
    private docRef: any;
    private collectionRef: any;

    /**
     * @constructor Set required properties
     * @param nodeTaskId node task id
     * @param userCode user's email
     * @param port port user is logged in
     */
    constructor(nodeTaskId: string, userCode: string, port: string) {
        this.nodeTaskId = nodeTaskId;//this.getUrlParameter("node_task_id");
        this.userCode = userCode; //this.getUrlParameter("usercode");
        this.port = port; //this.getUrlParameter("port");
        this.docRef = db.collection("tasks").doc(this.nodeTaskId);
        this.collectionRef = this.docRef.collection('submit_data').doc('key1').collection("annotations");
    }

    /**
     * Internal Function attempts to generate data at firebase
     * @param {string} nodeTaskId
     * @param {string} userCode
     * @returns {Promise} Promise object
     */
    private reAttempt(nodeTaskId: string, userCode: string): Promise<any> {
        return new Promise((resolve, reject) => {
            $.ajax({
                url:
                    process.env.PLATFORM_POPULATE_TASK + "?nodeTaskId=" + nodeTaskId + "&userCode=" + userCode,
                type: "GET",
                success: function (res) {
                    if (res && res.success) {
                        resolve(res);
                    } else {
                        reject(res);
                    }
                },
                error: function (err) {
                    reject(err);
                }
            });
        });
    }

    /**
     * Internal function creates data if not exists on firebase
     * @param {string} nodeTaskId
     * @param {string} userCode
     * @returns {Promise} promise object
     */
    private createDataIfNotExist(nodeTaskId: string, userCode: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.docRef
                .get()
                .then((doc: any) => {
                    if (!doc.exists) {
                        this.reAttempt(nodeTaskId, userCode)
                            .then(res => {
                                resolve({ success: true });
                            })
                            .catch(err => {
                                alert("Failed to create data on fb store");
                                reject(err);
                            });
                    } else {
                        resolve({ success: true });
                    }
                })
                .catch((e: any) => {
                    reject(e);
                });
        });
    }

    /**
     * Internal function Loads Frame Data from firebase
     * @param {string} nodeTaskId
     * @returns {Promise} promise object
     */
    private loadPartialData(nodeTaskId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.collectionRef.get().then((queySnapshot: any) => {
                const annotations: any[] = [];
                queySnapshot.forEach((doc: any) => {
                    let data;
                    try {
                        data = doc.data();
                        data.Frame_no = doc.id;
                        if (data.judgements && data.judgements.object && data.judgements.object.annotateddata) {
                            if (typeof data.judgements.object.annotateddata === "string") {
                                data.judgements.object.annotateddata = JSON.parse(data.judgements.object.annotateddata);
                            }
                        }
                        if (data.judgements && data.judgements.object && data.judgements.object.comment && data.judgements.object.comment.predefinedComment) {
                            if (typeof data.judgements.object.comment.predefinedComment === "string") {
                                data.judgements.object.comment.predefinedComment = JSON.parse(data.judgements.object.comment.predefinedComment);
                            }
                        }
                    } catch (e) {
                        AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, 'Error parsing json', e);
                    };
                    if (data) {
                        annotations.push(data);
                    }
                });
                return annotations;
            }).then((annotations: any[]) => {
                resolve(annotations);
            }).catch((err: any) => { reject(err) });
        });
    }

    /**
     * Sets user's info on firebase
     * @param {string} userCode user's email
     * @param {string} port port number on which user is connected
     * @returns {Promise} promise object
     */
    private setUserInfo(userCode: string, port: string): Promise<any> {
        return this.docRef.update({ userCode: userCode, port: port });
    }

    /**
     * Loads tasks data from firebase
     * @param {string} nodeTaskId
     * @param {string} userCode
     * @returns {Promise} promise object containing tasks data
     */
    loadData(nodeTaskId: string, userCode: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.createDataIfNotExist(nodeTaskId, userCode).then(res => {
                this.docRef.get().then((doc: any) => {
                    const task_data = doc.data();
                    let result = {};
                    try {
                        result = JSON.parse(task_data.result);
                    } catch (e) {
                        reject(new Error("Task data is not a valid JSON object"));
                    }
                    // set user info
                    this.setUserInfo(userCode, this.port).then(r => r).catch(e => {
                        AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, "Failed to set user info in firebase");
                    });
                    resolve(result)
                }).catch((err: any) => {
                    reject(err);
                })
            }).catch(e => {
                // thought this is not possible reject anywawy
                reject(e);
            });
        });
    }

    /**
     * Sets Frame data in firebase
     * @param key frame Number
     * @param data object should contain annotations if isFrame is not null
     * @param isFrame default null
     */
    setData(key: string, data: any, isFrame = null): Promise<any> {
        // if data has annotations stringify as fb don't has support for nested arrays
        // if (data.hasOwnProperty('drawable_attribute')) {
        //     let drawableKey :any = {};
        //     (data as AnnotationStateSerializedData).drawable_attribute.forEach((val: [string, any], idx: number)=> {
        //         drawableKey['drawattr_'+idx] = val;
        //     })
        //     let taxonomyKey :any = {};
        //     (data as AnnotationStateSerializedData).taxonomy_attribute.forEach((val: [string, string], idx: number)=> {
        //         taxonomyKey['taxonomyattr_'+idx] = val;
        //     })
        //     data.drawable_attribute = drawableKey;
        //     data.taxonomy_attribute = taxonomyKey;

        // }
        if (!isFrame) {
            let docNo = "";
            if (key.includes('formInput')) {
                docNo = key.split('_')[1]
            } else {
                docNo = key.split('_')[0]
            }
            this.collectionRef.doc(docNo).set({"Frame_no": docNo});
            return this.collectionRef.doc(docNo).collection('judgements').doc(key)
                .set(data);
        } else {
            let unMutedData = JSON.parse(data); // correct the JSON data
            return this.docRef
                .collection("active_frame")
                .doc(key + "")
                .set(unMutedData);
        }
    }

    /**
     * Gets frame data
     * @param {string} key Frame Number
     * @returns {Promise} promise objecct containing frame data
     */
    getData(key: string): Promise<any> {

        return this.collectionRef.doc(key).collection('judgements').get().then((queySnapshot: any) => {
            const judgements: any = {};
            queySnapshot.forEach((doc: any) => {
                judgements[doc.id] = doc.data()
                if (judgements[doc.id].hasOwnProperty('drawable_attribute')) {
                    let drawableKey : Array<[string, any]> = new Array<[string, any]>();
                    for (let idx=0; idx<Object.keys(judgements[doc.id].drawable_attribute).length; idx++) {
                        drawableKey.push(judgements[doc.id].drawable_attribute['drawattr_'+idx])
                    }

                    let taxonomyKey : Array<[string, string]> = new Array<[string, string]>();
                    for (let idx=0; idx<Object.keys(judgements[doc.id].taxonomy_attribute).length; idx++) {
                        drawableKey.push(judgements[doc.id].taxonomy_attribute['taxonomyattr_'+idx])
                    }
                    judgements[doc.id].drawable_attribute = drawableKey;
                    judgements[doc.id].taxonomy_attribute = taxonomyKey;
                }
            });
            return judgements;
        })
    }

    /**
     * Sets Active Frame in firebase
     * @param {string} frameNo current frame number
     * @returns {Promise} promise object
     */
    setActiveFrame(frameNo: string, updateTimeStamp: number): Promise<any> {
        return this.docRef.update({ activeFrame: frameNo, updateTimeStamp: updateTimeStamp });
    }

    /**
     * Gets current active frame as per data saved in firebase
     * @returns {Promise} object {activeFrame:string;updateTimestamp:Date}
     */
    getActiveFrame(): Promise<{ activeFrame: string; updateTimestamp: number }> {
        return this.docRef.get().then((doc: any) => doc.data()).then((data: any) => {
            let { activeFrame, updateTimestamp } = data;
            if (!activeFrame) {
                activeFrame = 1;
            }
            if (!updateTimestamp) {
                updateTimestamp = 0;
            }
            return { activeFrame, updateTimestamp };
        });
    }

    /**
     * Sets Submit status in firebase
     * @param {string} status submit status
     * @param {boolean} next load next task
     * @returns {Promise} promise object
     */
    setSubmitStatus(status: string, next: boolean): Promise<any> {
        return this.docRef.update({
            submit_status: status,
            next: next
        });
    }

}
