import  mongoDbClient, {stitchClient} from "./stitch";
import { AlertManager } from "../../components/alert-element/alert-element";
import { ConfigManager } from "../configservice/configmanager";
import {Config} from '../configservice/config';
import { RemoteMongoDatabase, RemoteMongoCollection } from "mongodb-stitch-browser-sdk";
import { addActiveSession, removeActiveSession } from "./stitch";
import { MessageBus } from "../messagebus/messagebus";
import { Activity } from "../messagebus/activity";
import { Operation } from "../messagebus/operation";
/**
 * Handles all stitch data transactions
 */
export default class Stitch {
    private engagement: string;
    private job : string;
    private node: string;
    private docRef: RemoteMongoDatabase|undefined;
    private judgementsRef: RemoteMongoCollection<{}>|undefined;
    private nodeBatchIdsRef: RemoteMongoCollection<{}>|undefined;
    private batchId : string|undefined;
    private showPopUp : boolean = true;

    /**
     * @constructor Set required properties
     * @param engagement engagement
     * @param job job
     * @param node nodeid
     */
    constructor(engagement: string, job:string, node:string) {
        this.engagement = engagement;
        this.job = job;
        this.node = node;
    }


    /**
     * @private
     * Prepare the Stitch SDK
     */
    private awaitInitialization() {
        return mongoDbClient().then(
            client => {
                this.docRef = client.db(this.engagement);
                this.judgementsRef = this.docRef.collection('judgement_' + this.job);
                this.nodeBatchIdsRef = this.docRef.collection('node_wise_batch_details_'+ this.job);
            }
        ).catch(err => {
            const msg = 'You have been disconnected due to the network, please try again.';
            console.log("DEBUG: ", err)
            throw new Error(msg)
        });
    }

    /**
     * Loads tasks data from firebase
     * @param {string} nodeTaskId
     * @param {string} userCode
     * @returns {Promise} promise object containing tasks data
     */
    loadData(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.awaitInitialization().then(() => {
                // Call a function to call the node_wise_batch_details_id
                stitchClient.callFunction('getBatch', []).then((batch: any) => {
                    if (batch === null) {
                        // @ts-ignore
                        AlertManager.processPopUpAlerts("Thank You", "The test has been completed. Please click on close.", "img/done.png", () => window.close());
                        reject(new Error("No more tasks available in selected nodes"));
                    } else {
                        this.batchId = batch;
                        resolve(batch);
                    }
                }).catch((err: any) => {
                    reject(err);
                    console.log("Encountered error inloadData")
                    // @ts-ignore
                    AlertManager.processPopUpAlerts("Tasking Information", "You have been disconnected due to the network, please try again.", "img/cry.png", () => window.close());
                })
            }).catch(e => {
                // thought this is not possible reject anyway
                // @ts-ignore
                AlertManager.processPopUpAlerts("Tasking Information", e, "img/cry.png", () => window.close());
                reject(e);
            });
        });
    }

    /**
     * @private
     * Preparing data for syncing with upstream store
     * @param key Index for database
     * @param data Data
     */
    private prepareData(key: string, data: any) {
        console.log('debugX prepareData: {key: ', key, ', data: ', data, '}')
        console.log('debugX judgementsRef: ', this.judgementsRef)

        let docNo = "";
        if (key.includes('formInput')) {
            docNo = key.split('_')[1]
        } else {
            docNo = key.split('_')[0]
        }
        data.node_wise_batch_details_id = (this.batchId as any)._id;
        data.key = key;
        data.seq_no = docNo;
    }

    /**
     * Sets Frame data in firebase
     * @param key frame Number
     * @param data object should contain annotations if isFrame is not null
     * @param isFrame default null
     */
    setData(key: string, data: any): Promise<any> {
        this.prepareData(key, data);
        return this.judgementsRef!.updateOne({node_wise_batch_details_id: data.node_wise_batch_details_id, seq_no: data.seq_no, key: data.key}, data, {upsert:true})
        .catch(e => {
            MessageBus.messageBusSubject.next(new Activity(Operation.EXIT_FINALISED));
            if (e.underlyingError.message === "Network request failed" && this.showPopUp){
                this.showPopUp = false;
                // @ts-ignore
                AlertManager.processPopUpAlerts("Tasking Information", "Network failure, please try after sometime", "img/cry.png");
            }
            // @ts-ignore
            return Promise.reject();
        })
    }


    /**
     * Sets Frame data in Bulk in mongodb
     * @param key frame Number
     * @param data object should contain annotations if isFrame is not null
     * @param isFrame default null
     */
    setBulkData(key: Array<string>, data: Array<any>): Promise<any> {
        key.forEach((k, indx) => {
            this.prepareData(k, data[indx])
        })
        return this.judgementsRef!.insertMany(data);
    }

    /**
     * Gets frame data
     * @param {string} frameno Frame Number
     * @returns {Promise} promise objecct containing frame data
     */
    getData(frameno: string): Promise<any> {
        return  stitchClient.callFunction('fetchTaskData', [(this.batchId as any)._id, frameno])
        .then(taskids => {
            return this.judgementsRef!.find({_id : {$in : taskids}})
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              .then((cursor:any) => cursor.toArray())
        .then((queySnapshot: any) => {
            const judgements: any = {};
            queySnapshot.forEach((doc: any) => {
                let key = doc.key;
                delete(doc.node_wise_batch_details_id);
                delete(doc.seq_no);
                delete(doc.key);
                judgements[key] = doc;

                if (judgements[key].hasOwnProperty('drawable_attribute')) {
                    let drawableKey : Array<[string, any]> = new Array<[string, any]>();
                    for (let idx=0; idx<Object.keys(judgements[key].drawable_attribute).length; idx++) {
                        drawableKey.push(judgements[key].drawable_attribute['drawattr_'+idx])
                    }

                    let taxonomyKey : Array<[string, string]> = new Array<[string, string]>();
                    for (let idx=0; idx<Object.keys(judgements[key].taxonomy_attribute).length; idx++) {
                        drawableKey.push(judgements[key].taxonomy_attribute['taxonomyattr_'+idx])
                    }
                    judgements[key].drawable_attribute = drawableKey;
                    judgements[key].taxonomy_attribute = taxonomyKey;
                }
            });
            return judgements;
        }).catch(err => {
            console.log("Error found in getData(): ", err)
            // @ts-ignore
                AlertManager.processPopUpAlerts("Tasking Information", "No More Task Available, try after sometime", "img/cry.png", () => window.close())
        });
    }

    /**
    * Sets Active Frame in firebase
    * @param {string} frameNo current frame number
    * @returns {Promise} promise object
    */
    setActiveFrame(frameNo: string, updateTimeStamp: number, lastReachFrame: number): Promise<any> {
        return this.nodeBatchIdsRef!.updateOne({_id: (this.batchId as any)._id}, {$set:{ activeFrame: frameNo, updateTimeStamp: updateTimeStamp, lastReachFrame: lastReachFrame }}, {upsert:true}).catch(e => {
            if(e.message === "invalid session"){
            // @ts-ignore
                AlertManager.processPopUpAlerts("Tasking Information", "Cannot open same task in two browsers, please close one and refresh the other.", "img/cry.png", () => window.close())
            }
        })
    }

    setTimerValue(timerValue: string): Promise<any> {
        console.log('debugX Reached setTimerValue inside index.tsx')
        console.log('SAVING TIMER KEY HERE: ', this.nodeBatchIdsRef)
        return this.nodeBatchIdsRef!.updateOne({_id: (this.batchId as any)._id}, {$set:{ timerValue: timerValue}}).catch(e => {
            if(e.message === "invalid session"){
            // @ts-ignore
                AlertManager.processPopUpAlerts("Could not add timerValue to the MongoDB database.", "img/cry.png")
            }
        })
    }

    /**
     * Gets current active frame as per data saved in firebase
     * @returns {Promise} object {activeFrame:string;updateTimestamp:Date}
     */
    getActiveFrame(): Promise<{ activeFrame: string; updateTimestamp: number, lastReachFrame: number }> {
        return this.nodeBatchIdsRef!.findOne({_id: (this.batchId as any)._id}).then((data:any) => {
            let { activeFrame, updateTimestamp, lastReachFrame } = data;
            if (!activeFrame) {
                activeFrame = 1;
            }
            if (!updateTimestamp) {
                updateTimestamp = 0;
            }
            if (!lastReachFrame) {
                lastReachFrame = 1;
            }
            return { activeFrame, updateTimestamp, lastReachFrame };
        });
    }


    /**
     * Return node_wise_batch_details_d
     */
    getBatchId() {
        return "NB-" + (this.batchId as any)._id;
    }

    /**
     * Sets Submit status in firebase
     * @param {string} status submit status
     * @param {boolean} next load next task
     * @returns {Promise} promise object
     */
    setSubmitStatus(status: string, next: boolean): Promise<any> {
        let config  = (ConfigManager.getConfig() as Config.RootObject);
        return this.nodeBatchIdsRef!.updateOne(
            {_id: (this.batchId as any)._id},
            {$set:{ submit_status: status, next: next, ...config.submitFlags }},
            {upsert:true}
        ).catch(e => {
            MessageBus.messageBusSubject.next(new Activity(Operation.EXIT_FINALISED));
            // @ts-ignore
            AlertManager.processPopUpAlerts("Tasking Information", "Cannot submit at this moment, Please try after sometime.", "img/cry.png", () => window.close())
        })
    }


    handleUnloadEvent() {
        console.log("Handle unload event called");
        removeActiveSession();
    }
}
