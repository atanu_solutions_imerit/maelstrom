const config = {
    apiKey: (window as any).__env.FIREBASE_APIKEY,
    authDomain: (window as any).__env.FIREBASE_AUTHDOMAIN,
    databaseURL: (window as any).__env.FIREBASE_DATABASE_URL,
    projectId: (window as any).__env.FIREBASE_PROJECT_ID,
    storageBucket: (window as any).__env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: (window as any).__env.FIREBASE_MESSAGING_SENDER_ID,

    stitchAppId: (window as any).__env.STITCH_APP_ID,
    stitchMongoDBService : (window as any).__env.STITCH_MONGODB_SERVICE
    
  };

export default config;