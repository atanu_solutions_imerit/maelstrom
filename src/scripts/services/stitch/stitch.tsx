import { Stitch, RemoteMongoClient, CustomCredential } from 'mongodb-stitch-browser-sdk'
import config from "./config";
import { Utils } from '../../utils/common/utils';
import jwt from "jsonwebtoken"
import { AlertManager } from '../../components/alert-element/alert-element';

Stitch.initializeDefaultAppClient(config.stitchAppId!)
export const stitchClient = Stitch.defaultAppClient

let isAuthenticated = false;
let dbClient : RemoteMongoClient;

export function addActiveSession(user:any, token:string) {
    let decodedToken:any = jwt.decode(token);

    // TODO: This part was commented as this section was throwing errors for LND
    //       This needs to be looked into later
    //
    // if (user.isLoggedIn) {
    //     console.log("User is logged in")
    //     AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.WARNING_NOTIFICATION, "All Previous session has been logged out")
    // }
    console.log("addActiveSession2")

    let postData = {
        "engageCode": decodedToken['engagement'],
        "jobCode" : decodedToken['job'],
        "nodeId": decodedToken['node'],
        "empCode" : decodedToken['emp_code'],
        "uid" : user.id
    };
    console.log("addActiveSession3")

    return new Promise((resolve, reject) => {
        $.ajax({
            url: (window as any).__env.ADD_SESSION_ENDPOINT,
            crossDomain : true,
            method: "POST",
            data : postData,
            dataType: "json",
            success: function (data, status, jqXhr) {
                if (status) {
                    resolve(data.taskToken);
                } else {
                    reject(status);
                    AlertManager.processPopUpAlerts("Tasking Information", "Error in syncing login details", "img/cry.png", () => window.close());
                }
            },
            error: function (jqXhr, status, message) {
                reject(message);
                // @ts-ignore
                AlertManager.processPopUpAlerts("Tasking Information", "Error in syncing login details", "img/cry.png", () => window.close());
            }
        })
    })
}

export function removeActiveSession() {
    let token = Utils.getJWT();
    let user = Utils.getUser();

    let decodedToken:any = jwt.decode(token);
    let postData = {
        "engageCode": decodedToken['engagement'],
        "uid" : user
    };

    return new Promise((resolve, reject) => {
        $.ajax({
            url: (window as any).__env.REMOVE_SESSION_ENDPOINT,
            crossDomain : true,
            method: "POST",
            data : postData,
            dataType: "json",
            success: function (data, status, jqXhr) {
                if (status) {
                    resolve();
                } else {
                    reject(status);
                    AlertManager.processPopUpAlerts("Tasking Information", "Error in syncing login details", "img/cry.png", () => window.close());
                }
            },
            error: function (jqXhr, status, message) {
                reject(message);
                AlertManager.processPopUpAlerts("Tasking Information", "Error in syncing login details", "img/cry.png", () => window.close());
            }
        })
    })
}



export default function getStitchComponent() {
    if (isAuthenticated) {
        return Promise.resolve(dbClient);
    } else {
        let token = Utils.getJWT();
        return stitchClient.auth.loginWithCredential(new CustomCredential(token)).then(
            (user:any) => {
                Utils.setUser(user.id);

                return addActiveSession(user, token)
            }
        ).then(() => {
            dbClient = stitchClient.getServiceClient(RemoteMongoClient.factory, config.stitchMongoDBService!);
            isAuthenticated = true;
            return dbClient;
        })
        .catch((err:any) => {
            // @ts-ignore
            AlertManager.processPopUpAlerts("Tasking Information", "Session Expired. Please login through IMPP to start again.", "img/cry.png", () => window.close());
            console.error(err);
        });
    }
}

