/**
 * @file This file contains the Config Manager service 
 */
import { JSONValidator } from "../../utils/common/JSONValidator";
import { MessageBus } from "../messagebus/messagebus";
import { Operation } from "../messagebus/operation";
import { Activity } from "../messagebus/activity";
import { AlertManager } from "../../components/alert-element/alert-element";
import { Config } from "./config";

/**
 * Types of Config View available to consumer
 */
export enum ConfigTypes {
    ALL,
    CLASSIFICATION,
    NONE
}

/**
 * @class
 * This Static class manages the Configuration for this application run 
 */
export class ConfigManager {
    /**
     * @static
     * Handle for Live Data of the Config
     */
    public static configdata : Config.RootObject;

    /**
     * @static
     * Service gets initiated from this call
     */
    static start() {
        let config = this.fetchConfig()
        if ( this.validateConfig(config) === true ) {
            this.configdata = config;
        } else {
            AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, "Configuration does not matches its schema");
            MessageBus.messageBusSubject.next(new Activity(Operation.ACTION_CONFIG_INVALIDATED))
        }
                    
        MessageBus.messageBusSubject.next(
            new Activity(
              Operation.ACTION_CONFIG_FETCHED,
              undefined,
              undefined,
              this.configdata
          )
        );
    }


    /**
     * @static
     * AJV based JSON Validation 
     * @param config Data which needs validation
     */
    static validateConfig(config: Object) : boolean {
        const jsonSchema = require('./configschema.json')
        return JSONValidator.validateJSONSchema(jsonSchema, config)
    }

    /**
     * @static
     * API to download config from upstream store
     */
    static fetchConfig()  {
        let annotationClass = require('../../../data/annotationClass.json')
        let configOnly = require('../../../data/config.json')
        let config = { ...annotationClass,...configOnly}
        return config;
    }

    /**
     * API exposed to all application component to get data from this services
     * @param type ConfigTypes
     */
    static getConfig(type:ConfigTypes=ConfigTypes.ALL) : Object{
        
        if(this.configdata){
            if ( type === ConfigTypes.CLASSIFICATION) {
                return Object.assign({}, this.configdata.classification);
            } else {
                return Object.assign({},this.configdata);
            }
        } else {
            throw new Error("Config Not Found");
        }
    }

}