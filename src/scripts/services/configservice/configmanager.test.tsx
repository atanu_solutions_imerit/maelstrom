import { ConfigManager } from "./configmanager";
import { MessageBus } from "../messagebus/messagebus";
import { Operation } from "../messagebus/operation";
import { Activity } from "../messagebus/activity";
import { filter } from "rxjs/operators";
jest.mock("./configmanager.tsx");

describe("ConfigManager Test", () => {
  it("Config Manager should be initiated", () => {
    const confMngr = new ConfigManager();
    expect(ConfigManager).toHaveBeenCalledTimes(1);
  });

  it("Should fetch appropriate config", () => {
    // console.log(ConfigManager.fetchConfig());
    expect(ConfigManager.fetchConfig()).not.toBe('undefined');
    // expect(ConfigManager.start())
  });

  it('should validate config', () => {
    let config = { classification: 
      { classification: 'type',
        entity: 
         [ {} ] },
     attribute: 
      [ { name: 'occluded', value: 'binary' },
        { name: 'truncated', value: 'binary' } ],
     selectableComment: 
      { commentOptions: [ {} ],
        imageLabel: [ {} ],
        depth: [ {} ] },
     note: true,
     grid_pixel: 30,
     static_data_url: 'data/',
     clipboard_store: 'CLIPBOARD',
     annotation: 
      { prepopulateClassification: true,
        reload: true,
        drawable: { polygon: {}, bbox: {}, keypoint: {} } },
     local_cache: { dbname: 'LightningDB', objectstore: 'tasks', windowsize: 15 } }

    expect(ConfigManager.validateConfig(config)).toBeTruthy();
  });

});
