/**
 * @file This File contains all interfaces in Config Namespace used
 */
export namespace Config {

    export interface IPolygon {
        enable: boolean;
        fillOpacity: number;
        crosshairEnable: boolean;
        truncateEnable: boolean;
        metaInfo: IPolygonMetaInfo;
    }

    export interface IPolyline {
        enable: boolean;
        fillOpacity: number;
        crosshairEnable: boolean;
    }

    export interface IBboxMetaInfo {
        diagonal: boolean;
    }

    export interface IPolygonMetaInfo {
        vertexCount: boolean;
    }

    export interface IBbox {
        enable: boolean;
        fillOpacity: number;
        crosshairEnable: boolean;
        truncateEnable: boolean;
        minEdge: number;
        minArea: number;
        minDiagonal: number;
        metaInfo: IBboxMetaInfo;
    }

    export interface IKeypoint {
        enable: boolean;
        fillOpacity: number;
        crosshairEnable: boolean;
        radius: number;
    }

    export interface IDrawable {
        polygon: IPolygon;
        bbox: IBbox;
        keypoint: IKeypoint;
        polyline: IPolyline;
    }

    export interface IAnnotation {
        reload: boolean;
        drawable: IDrawable;
    }

    export interface ILocalCache {
        dbname: string;
        objectstore: string;
        windowsize: number;
    }

    export interface IAttribute {
        name: string;
        value: string;
    }

    export interface Option {
        value: string;
        text: string;
    }

    export interface ISelectableComment {
        name: string;
        dataPlaceholder: string;
        default: string;
        options: Option[];
    }


    export interface IMultiSelectableComment {
        name: string;
        dataPlaceholder: string;
        default: string;
        options: Option[];
    }

    export interface IEntity2 {
        id: number;
        Parent: string;
        name: string;
    }

    export interface IChild {
        classification: string;
        entity: IEntity2[];
    }

    export interface IEntity {
        id: number;
        name: string;
        shape: string[];
        classChild: string[];
        color: string;
        toplevel: boolean;
        child: IChild;
    }

    export interface IClassification {
        classification: string;
        entity: IEntity[];
    }

    export interface RootObject {
        submitFlags: any;
        annotation: IAnnotation;
        crossHairColor: string;
        gridPixel: number;
        localCache: ILocalCache;
        prepopulateClassification: boolean;
        attribute: IAttribute[];
        note: boolean;
        selectableComment: ISelectableComment;
        multiSelectableComment: IMultiSelectableComment;
        classification: IClassification;
    }

}
