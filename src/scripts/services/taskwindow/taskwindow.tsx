/**
 * @file This file houses definitiion of a single class viz, TaskWindow (Service) which manages the Local Storage with Windowing for partial
 * data caching.
 */
import { AlertManager } from "../../components/alert-element/alert-element";
import { filter } from "rxjs/operators";
import { MessageBus } from "../messagebus/messagebus";
import { Activity } from "../messagebus/activity";
import { Operation } from "../messagebus/operation";
import * as _ from 'lodash'
import { TaskAudio } from "../messagebus/taskinfo";
import { TSMap } from "typescript-map";
import { FormInput } from "../../views/formpanel/formpanel_state";
import { TaskInfo } from "../messagebus/taskinfo";
import { ConfigManager } from "../configservice/configmanager";
import { LoaderManager } from "../../components/loader-element/loader-element";
import { TaskData, SerializedData, ActiveFrameSerializedData, MediaSerializedData, TimeLineSerializedData } from "../messagebus/taskdata";
import { Config } from "../configservice/config";
import { MediaState } from "../../views/media/media_state";

/**
 * @typedef
 * TypeDefinition of CallBack which takes TaskData as argument without returning anything
 */
declare type LocalFetchDataHandler = (args : TaskData) => void;

/**
 * @class
 * IndexedDB based Sliding Window implementation for Caching AnnotationData and CommentData in a batch
 */
export class TaskWindow {
    /**
     * @private
     * Defaults value for IndexedDB Table name gets overrided from Config
     */
    private static OBJECTSTORE = "tasks";
    /**
     * @private
     * Defaults value for window size of the number of  gets overrided from Config
     */
    private static WINDOWSIZE = 2000;
    /**
     * @private
     * Handle for IndexedDB Database
     */
    private static db : IDBDatabase|undefined = undefined
    /**
     * @private
     * Key used for identifying a set of annotations
     */
    private static _key : number = -1
    /**
     * @private
     * List of all key denoting the sets of annotations that has been synced
     */
    private static syncList : Array<number> = []
    /**
     * @private
     * Map between key used for a set of annotations and its corresponding Task Object
     */
    private static _taskList : TSMap<number, TaskAudio>|undefined;
    /**
     * @private
     * List of frame which needs syncing
     */
    private static _dirtyList : Set<string> = new Set<string>();
    /**
     * @private
     * Syncing in progress
     */
    private static _syncInProgress : string = "";
    /**
     * @private @static
     * Batch Information only required field may be later required
     */
    private static _batchId : string = ''

    /**
     * @private @static
     * Flag to denote db is open or close
     */
    private static _dbOpen : boolean = false;

    /**
     * @static
     * Initializes the IndexedDB database once for this application and also register some Event Listeners
     * on the global MessageBus to react accordingly.
     */
    static start() {
        this.syncList = []
        this._dbOpen = false;

        let config = (ConfigManager.getConfig() as Config.RootObject);
        if ( config.localCache === undefined || config.localCache.dbname === undefined
            || config.localCache.objectstore === undefined || config.localCache.windowsize === undefined ) {
            AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, 'Configuration required for IndexedDB setup is missing');
            return;
        }
        TaskWindow.OBJECTSTORE = config.localCache.objectstore;
        TaskWindow.WINDOWSIZE = config.localCache.windowsize;
        if (!window.indexedDB) {
            AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, 'Browser dont support a stable version of indexedDB');
            return;
        }

        window.onbeforeunload = () => {
            MessageBus.messageBusSubject.next(new Activity(Operation.PAGE_UNLOAD));
            if ( this._syncInProgress !== "" || this._dirtyList.size !== 0) {
                AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, "Browser window closing temporarily stopped for unfinished syncing");
                return "Do you really want to leave our brilliant application?";
            } else {
                return;
            }

         };

        MessageBus.messageBusObservables
        .pipe(filter(a => a.action === Operation.ACTION_FETCH_BATCH))
        .subscribe(a => {
            this._taskList = a.poststate.taskList
            this._batchId = a.poststate.batchIdentifier;
            let upstreamActiveFrame = a.prestate as ActiveFrameSerializedData;

            let openRequest = window.indexedDB.open(config.localCache.dbname + this._batchId);

            new Promise((resolve, reject) => {
                openRequest.onerror = (event) => {
                    AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, 'Accessing IndexedDB is not allowed for this app')
                    reject()
                }

                openRequest.onsuccess = (event:any) => {
                    this.db = event.target.result
                    this._dbOpen = true;
                    resolve()
                }

                openRequest.onupgradeneeded = (event: any) => {
                    this.db = event.target.result;
                    let objectstore = this.db!.createObjectStore(TaskWindow.OBJECTSTORE)
                }

            }).then(() => {
                this.readBatchData().then(localActiveFrame => {
                    if (upstreamActiveFrame === undefined && localActiveFrame === undefined) {
                        this._key = 1
                    } else if(upstreamActiveFrame === undefined) {
                        this._key = localActiveFrame.activeFrame
                    } else if(localActiveFrame === undefined) {
                        this._key = upstreamActiveFrame.activeFrame
                    } else {
                        this._key = upstreamActiveFrame.updated_on >  localActiveFrame.updated_on ? upstreamActiveFrame.activeFrame : localActiveFrame.activeFrame;
                    }
                    this.pullTaskFromUpstream()
                    this.checkForSpace()

                    if ( this.syncList.indexOf(this._key) !== -1 ) {
                        this.fetchTaskDataFromLocalDB(this._key)
                        MessageBus.messageBusSubject.next(new Activity(Operation.ACTION_LOAD_TASK_REQUEST, undefined, this._key, this._taskList!.get(this._key)))
                        this.writeBatchData(new ActiveFrameSerializedData(this._key))
                    }
                })
            }).catch(err => {
                AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, "ReadBatchData failed ", err)
            })
            //Place to find out landing index right now default to 1
        });

        MessageBus.messageBusObservables
        .pipe(filter(a => a.action === Operation.ACTION_PULL_TASK_DATA_RESPONSE))
        .subscribe(a => {
            //Prestate contains the FrameNo and Poststate contains all the annotations in Array
            let serializedData = a.poststate as SerializedData;
            this.updateTaskData(a.prestate, serializedData.data)
        })

        MessageBus.messageBusObservables
        .pipe(filter(a => a.action === Operation.PLAYBACK_CHANGE))
        .subscribe(a => {
            this.writeTaskData((a.poststate as MediaSerializedData), TaskWindow.DATA_TYPE.MEDIADATA);
        });

        MessageBus.messageBusObservables
        .pipe(filter(a => a.action === Operation.TIMELINEDATASTART))
        .subscribe(a => {
            this.writeTaskData((a.poststate as TimeLineSerializedData), TaskWindow.DATA_TYPE.TIMELINEDATA);
        });

        MessageBus.messageBusObservables
        .pipe(filter(a => a.action === Operation.TIMELINEDATAEND))
        .subscribe(a => {
            this.writeTaskData((a.poststate as TimeLineSerializedData), TaskWindow.DATA_TYPE.TIMELINEDATA);
        });

        MessageBus.messageBusObservables
            .pipe(filter(a => a.action === Operation.COMMENT_CHANGE))
            .subscribe(a => {
                this.writeTaskData((a.poststate as FormInput), TaskWindow.DATA_TYPE.FORMINPUT);
            })

        MessageBus.messageBusObservables
            .pipe(filter(a => a.action === Operation.PREV_NAV_ALLOWED))
            .subscribe(a => {
                this.prevTask()
            })


        MessageBus.messageBusObservables
            .pipe(filter(a => a.action === Operation.NEXT_NAV_ALLOWED))
            .subscribe(a => {
                this.nextTask()
            })

        MessageBus.messageBusObservables
            .pipe(filter(a => a.action === Operation.GOTO_NAV_ALLOWED))
            .subscribe(a => {
                this.gotoTask(a.poststate)
            })

        MessageBus.messageBusObservables.pipe(filter(a=>a.action=== Operation.TASK_SUBMIT_ALLOWED))
            .subscribe(a=>{
                LoaderManager.processLoading(LoaderManager.LOADING_TYPE.USER_SUBMIT_NOTIFICATION, null, 'show');
                //this.syncOperation(config)
            });

        MessageBus.messageBusObservables.pipe(filter(a => a.action === Operation.ACTION_PUSH_TASK_DATA_SUCCESS ))
            .subscribe(a => {
                // LoaderManager.processLoading(LoaderManager.LOADING_TYPE.USER_SUBMIT_NOTIFICATION, this._dirtyList.size, 'update');
                this._syncInProgress = "";
                this.pushTaskData()
            })

        MessageBus.messageBusObservables.pipe(filter(a => a.action === Operation.ACTION_PUSH_TASK_DATA_FAILURE ))
            .subscribe(a => {
                // LoaderManager.processLoading(LoaderManager.LOADING_TYPE.USER_SUBMIT_NOTIFICATION, this._dirtyList.size, 'update');
                let val = this._syncInProgress
                this._syncInProgress = ""
                this.pushTaskData(val)
            })
    }

    /**
     * @private @static
     * Wrapper over taskSwitcher for Prev Frame Navigation
     */
    private static prevTask()  {
        if (this._key > 1) {
            this._key--;
            this.taskSwitcher();
        }
    }

    /**
     * @private @static
     * Wrapper over taskSwitcher for Next Frame Navigation
     */
    private static nextTask() {
        if (this._key < this._taskList!.length) {
            this._key++;
            this.taskSwitcher();
        }
    }

    /**
     * @private @static
     * Wrapper over taskSwitcher for Moving to particular Frame
     */
    private static gotoTask(num:number) {
        if (num >= 1 && num <=this._taskList!.length) {
            this._key = num;
            this.taskSwitcher()
        }
    }

    /**
     * @private @static
     * Function disables all UserInput, Pull Task from Upstream if required for Sliding Window,
     * Check for Local Space in IndexedDB and if the required frame is already available sent
     * LoadRequest to other modules to load that task
     */
    private static taskSwitcher() {
        this.pullTaskFromUpstream()
        this.checkForSpace()
        if ( this.syncList.indexOf(this._key) !== -1 ) {
            this.fetchTaskDataFromLocalDB(this._key)
            MessageBus.messageBusSubject.next(new Activity(Operation.ACTION_LOAD_TASK_REQUEST, undefined, this._key, this._taskList!.get(this._key)))
            this.writeBatchData(new ActiveFrameSerializedData(this._key))
        }
    }


    /**
     * @private @static
     * Prepares the list of TaskData which needs to be download and advertise the request
     */
    private static async pullTaskFromUpstream() {
        let indices = this.checkForRequiredFrame(this._key, this._taskList!.length)
        let data = new TSMap<Number, TaskAudio>();
        indices.forEach( a => {
            data.set(a, this._taskList!.get(a))
        })
        MessageBus.messageBusSubject.next(new Activity(Operation.ACTION_PULL_TASK_DATA, undefined, undefined, data))

    }


    private static readBatchData() : Promise<ActiveFrameSerializedData> {
        if (!this._dbOpen) {
            AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, "IndexedDB is not available");
            return new Promise((resolve, reject) => reject('IndexedDB closing'));
        }
        let transaction = this.db!.transaction([TaskWindow.OBJECTSTORE], "readonly")
        let objectstore = transaction.objectStore(TaskWindow.OBJECTSTORE)
        let data : TaskData = new TaskData();
        let valueList : Array<any> = new Array<any>();

        let readRequest = objectstore.get('activeFrame')
        return new Promise((resolve, reject) => {
            readRequest.onsuccess = (event:any) => {
                resolve(event.target.result)
            };

            readRequest.onerror = (event:any) => {
                reject(new Error("ActiveFrame not found"))
            };
        })

    }

    private static writeBatchData(data : ActiveFrameSerializedData) {
        if (!this._dbOpen) {
            return AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, "IndexedDB is not available");
        }
        let transaction = this.db!.transaction([TaskWindow.OBJECTSTORE], "readwrite")
        let objectstore = transaction.objectStore(TaskWindow.OBJECTSTORE)
        //let sData = (data as FormInput).serializedData
        objectstore.put(data, 'activeFrame')
        transaction.onerror = (error:any) => AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, "Particular batch information might not exists")
        transaction.oncomplete = (event:any) => {}
    }

    /**
     * @private @static
     * Function to write data in IndexedDB for local Caching
     * @param data Item that needs write to IndexedDB
     * @param isAnnotation Type of Data whether Annotation/FormInput
     */
    private static writeTaskData(data : MediaState|FormInput|MediaSerializedData,type:TaskWindow.DATA_TYPE) {
        if (!this._dbOpen) {
            return AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, "IndexedDB is not available");
        }

        let transaction = this.db!.transaction([TaskWindow.OBJECTSTORE], "readwrite")
        let objectstore = transaction.objectStore(TaskWindow.OBJECTSTORE)

        if ( type == TaskWindow.DATA_TYPE.MEDIADATA) {
            let sData = (data as MediaSerializedData);
            //console.log(sData);
            objectstore.put(sData, 'mediaInput_' + this._key)
            this.pushTaskData('mediaInput_' + this._key)
        }

        else if ( type == TaskWindow.DATA_TYPE.TIMELINEDATA) {
            let sData = (data as TimeLineSerializedData);
            //console.log(sData);
            objectstore.put(sData, 'timeline_' + this._key);
            this.pushTaskData('timeline_' + this._key);
        }
        else if ( type == TaskWindow.DATA_TYPE.TRANSCRIPTION) {
        }
        else if( type == TaskWindow.DATA_TYPE.FORMINPUT) {
            let sData = (data as FormInput).serializedData
            objectstore.put(sData, 'formInput_' + this._key)
            this.pushTaskData('formInput_' + this._key)
        }

        //TaskManager needs to be informed about this dirty bits
        transaction.onerror = (error:any) => AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, "Particular key " + this._key + "_" + " might not exists")
        transaction.oncomplete = (event:any) => {}
    }

    /**
     * @private @static
     * Data which comes from Upstream Server comes to this function to be written in IndexedDB if it contains
     * latest data compare to already existing one.
     * @param key Key used for identifying a set of annotations
     * @param data Composite structure containing annotations detail and Comments and ImageMetaInfo
     */
    private static updateTaskData(key: number, data: TaskData) {
        if (!this._dbOpen) {
            return AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, "IndexedDB is not available");
        }

        //console.log("UPdate Task Data ", data);
        let transaction = this.db!.transaction([TaskWindow.OBJECTSTORE], "readwrite")
        let objectstore = transaction.objectStore(TaskWindow.OBJECTSTORE)
        transaction.onerror = (error:any) => AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, "Particular key " + key + " might not exists")
        transaction.oncomplete = (event:any) => {}
        data.transcribedText.forEach(upstreamData => {
            let fetchRequest = objectstore.get(key + '_' + upstreamData.id)
            fetchRequest.onsuccess = (event:any) => {
                let storedData = event.target.result
                if (storedData === undefined) {
                    let insertRequest = objectstore.put(upstreamData, key + '_' + upstreamData.id)
                } else {
                    if (upstreamData.updated_on > storedData.updated_on) {
                        let updateRequest = objectstore.put(upstreamData, key + '_' + upstreamData.id)
                    }
                }
            };
            fetchRequest.onerror = (error:any) => {AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, error)};
        })

        let fetchRequest = objectstore.get('formInput_' + key)
        fetchRequest.onsuccess = (event:any) => {
            let storedData = event.target.result
            if (storedData === undefined) {
                if (data.comments.updated_on === undefined) {
                    data.comments.updated_on = 0
                }
                let insertRequest = objectstore.put(data.comments, 'formInput_' + key)
            } else {
                if (data.comments.updated_on > storedData.updated_on) {
                    let updateRequest = objectstore.put(data.comments, 'formInput_' + key)
                }
            }
        };
        fetchRequest.onerror = (error:any) => {AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, error)};

        if ( this.syncList.indexOf(key) === -1 ) {
            this.syncList.push(key)
            this.syncList.sort((a,b)=> a-b);
        }
        if ( this._key === key ) {
            this.fetchTaskDataFromLocalDB(this._key)
            MessageBus.messageBusSubject.next(new Activity(Operation.ACTION_LOAD_TASK_REQUEST, undefined, this._key, this._taskList!.get(this._key)))
            this.writeBatchData(new ActiveFrameSerializedData(this._key))
        }
    }

    /**
     * @private @static
     * Task Data which comprise the annotation, comments, image MetaInfo are fetched when task is switched.
     * The key is already set from other function viz, nextTask, prevTask, gotoTask
     */
    private static fetchTaskDataFromLocalDB(key: number|string, localCallback ?: LocalFetchDataHandler) : void {
        if (!this._dbOpen) {
            AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, "IndexedDB is not available"); return;
        }

        let taskKey = IDBKeyRange.bound(key +'_', key + '_\uffff')
        let transaction = this.db!.transaction([TaskWindow.OBJECTSTORE], "readwrite")
        let objectstore = transaction.objectStore(TaskWindow.OBJECTSTORE)
        let valueList : Array<any> = new Array<any>();

        if (localCallback !== undefined) {
            let readRequest = objectstore.get(key)
            readRequest.onsuccess = (event:any) => {
                localCallback(readRequest.result)
            }
            readRequest.onerror = (event:any) => {AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, "Error in readRequest")}
            return
        }

        let readRequest = objectstore.get('formInput_' + key)
        readRequest.onsuccess = (event:any) => {
            MessageBus.messageBusSubject.next(new Activity(Operation.ACTION_FETCH_COMMENT, undefined, undefined, readRequest.result))
        }

        readRequest.onerror = (event:any) => {AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, "Error in readRequest")}

        objectstore.openCursor(taskKey).onsuccess = (event:any) => {
            let cursor = event.target.result;
            if (cursor) {
                if (valueList.length < TaskWindow.FETCH_BATCH_SIZE) {
                    valueList.push(cursor.value)
                } else {
                    MessageBus.messageBusSubject.next(new Activity(Operation.ACTION_FETCH, undefined, undefined, valueList))
                    valueList.splice(0, valueList.length)
                    valueList.push(cursor.value)
                }
                cursor.continue()
            } else {
                MessageBus.messageBusSubject.next(new Activity(Operation.ACTION_FETCH, undefined, undefined, valueList))
                let imageUrl = this._taskList!.get(key as number)._audioUrl;
                let batchSize = this._taskList!.length;
                let taskImage = new TaskAudio(imageUrl, true)
                let taskInfo = new TaskInfo(this._batchId, taskImage, batchSize, key as number);
                MessageBus.messageBusSubject.next(new Activity(Operation.ACTION_FETCH_COMPLETE, undefined, undefined, taskInfo))
            }
        }

    }

    /**
     * @private @static
     * Function clears all details of a particular key from indexDB cache when there is a resource crunch.
     * @param key Key used for identifying a set of annotations
     */
    private static removeData(key:number) {
        return new Promise<void>((resolve, reject) =>  {
            if (!this._dbOpen) {
                AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, "IndexedDB is not available");
                reject();
            }

            let taskKey = IDBKeyRange.bound(key +'_', key + '_\uffff')
            let transaction = this.db!.transaction([TaskWindow.OBJECTSTORE], "readwrite");

            // report on the success of the transaction completing, when everything is done
            transaction.oncomplete = (event) => {
                this.syncList.splice(this.syncList.indexOf(key), 1);
                resolve();
            };

            transaction.onerror = (event) => {
                reject();
            };

            let objectStore = transaction.objectStore(TaskWindow.OBJECTSTORE);
            objectStore.delete(taskKey);
        })
    }


    /**
     * @private @static
     * Checks for the index which needs download based on window size
     * @param index key of a task that needs to be downloaded
     * @param tasklength List of Task present in the present batch
     */
    private static checkForRequiredFrame(index:number, tasklength:number) {
        let indices = []
        //If we have touched this after last refresh then we dont download this value gets cleaned on task load
        for (let idx=Math.max(index-this.WINDOWSIZE,1); idx<=Math.min(index+this.WINDOWSIZE, tasklength); idx++) {
            if (! _.includes(this.syncList, idx) ) {
                indices.push(idx)
            }
        }
        return indices;
    }

    /**
     * @private @static
     * Checks whether enough space is left in IndexedDB to save more taskdata in local cache if there is
     * a resource crunch we try to remove task data entries which as farthest away from the sliding window
     */
    private static async checkForSpace() {
        var val = navigator.storage && navigator.storage.estimate ? await navigator.storage.estimate() : undefined;
        if (val === undefined || val.quota === undefined || val.usage === undefined) {
            AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, "Local storage is not present. Do not proceed, contact Local Admin.");
            return;
        }
        if (this.syncList.length === 0)        {
            return;
        }

        val.quota = 10.01 * 1024 * 1024;
        while ((val!.quota! - val!.usage!) < (10 * 1024 * 1024)) {
            if ( (this._key-this.WINDOWSIZE) > this.syncList[0] ) {
                await this.removeData(this.syncList[0])
            }
            if (this.syncList.length === 0) {
                return;
            }
            if ( (this._key+this.WINDOWSIZE) < this.syncList[this.syncList.length-1] ) {
                await this.removeData(this.syncList[this.syncList.length-1])
            }
            val = await navigator.storage.estimate()
        }
    }

    /**
     * @private @static
     * Request Registration for Pushing Frame data
     * @param key Optional sequence number
     */
    private static pushTaskData(key?: string) {
        if (key !== undefined) {
            this._dirtyList.add(key);
        }
        if (this._syncInProgress === "") {
            let _key = this._dirtyList.values().next().value;
            if (_key) {
                this._syncInProgress = _key;
                this.generateTaskData(_key);
                this._dirtyList.delete(_key)
            }
        }
    }


    /**
     * @private @static
     * This Function generates TaskData to be sent to Upstream Server
     * @param key Index in the batch
     */
    private static generateTaskData(key: string) {
        this.fetchTaskDataFromLocalDB(key, (data) => {
            MessageBus.messageBusSubject.next(
                new Activity(Operation.ACTION_PUSH_TASK_DATA, undefined, key, data));

        })
    }
}

export namespace TaskWindow {
    /** Batching fetch for quick task switching */
    export const FETCH_BATCH_SIZE = 400;

    export enum DATA_TYPE{
        MEDIADATA,
        FORMINPUT,
        TRANSCRIPTION,
        TIMELINEDATA,
    }
}
