/**
 * @file This files houses only a single class viz, DrawManager. Most of the SVG
 * related stuff are imported within this file
 */
import { filter, map } from 'rxjs/operators';

import { MessageBus } from '../messagebus/messagebus';
import { Operation } from '../messagebus/operation';
import { LoaderManager } from '../../components/loader-element/loader-element';
import { TextEditor } from '../../components/text-editor/text-editor';
import { AlertManager } from '../../components/alert-element/alert-element';
import { TranscribedTextSerializedData } from '../messagebus/taskdata';
import { TSMap } from 'typescript-map';

export class TranscriptionManager {
    private static _textEditor: TextEditor | undefined;
    private static _groupToSpanElem: TSMap<string, Array<TranscribedTextSerializedData>> = new TSMap<string, Array<TranscribedTextSerializedData>>();


    static start(div: string) {
        this._textEditor = new TextEditor();
        $('#' + div).append(this._textEditor.getView());

        MessageBus.messageBusObservables
            .pipe(filter(a => a.action === Operation.ACTION_FETCH_COMPLETE))
            .subscribe(a => {
                this._groupToSpanElem.forEach(spans => {
                    spans.sort((a, b) => a.seqno - b.seqno)
                    .forEach((a: TranscribedTextSerializedData) => {
                        if (a.deleted) return;
                        this._textEditor!.updateContent(a);
                    })
                });
                LoaderManager.processLoading(LoaderManager.LOADING_TYPE.PLAIN_NOTIFICATION, null, 'hide');
                AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.INFO_NOTIFICATION, "All annotations draw complete") // Remove the loader
            })

        MessageBus.messageBusObservables
            .pipe(filter(a => a.action === Operation.ACTION_FETCH))
            .pipe(map(a => a.poststate))
            .subscribe((list: Array<TranscribedTextSerializedData>) => {
                list.forEach(elem => {
                    let array = this._groupToSpanElem.get(elem.gid);
                    if (array === undefined) {
                        this._groupToSpanElem.set(elem.gid, [])
                        array = this._groupToSpanElem.get(elem.gid);
                    }
                    array.push(elem)
                })
            });
    }

}
