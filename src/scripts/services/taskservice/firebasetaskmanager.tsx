import { TaskManager } from "./taskmanager";
import { TSMap } from "typescript-map";
import { TaskImage, TaskAudio } from "../messagebus/taskinfo";
import { SerializedData, TaskData, ActiveFrameSerializedData, TranscribedTextSerializedData, FormInputSerializedData } from "../messagebus/taskdata";
import Firebase from "../firebase";
import { AlertManager } from "../../components/alert-element/alert-element";

/**
 * @class @extends TaskManager
 * Firebase support Implementation version does has Firebase as cache for Task and TaskData
 */
export class FirebaseTaskManager extends TaskManager {
    /**
     * @static
     * SingleTon handle for Firebase
     */
    static _firebaseHandle : Firebase;

    /**
     * @static 
     * Return the Promise for FirebaseTaskManager for Task list only
     * @returns Promise with nothing as value
     */
    static pullRawData() : Promise<any> {
        let nodeTaskId = Firebase.getUrlParameter('node_task_id')
        let userCode = Firebase.getUrlParameter('usercode')
        let port = Firebase.getUrlParameter('port')
        this._firebaseHandle = new Firebase(nodeTaskId, userCode, port)
        return this._firebaseHandle.loadData(nodeTaskId, userCode)
    }

    

    /**
     * @static
     * Firebase TaskManager generates dataset from upstream server
     * @param data Data downloaded for parsing
     */
    static parseRawData(data: any) : {batchIdentifier: string, taskList : TSMap<number, TaskAudio>}{
        let ret = new TSMap<number, TaskAudio>();
        let taskList : Array<any> = [];
        for (let iter=0; iter<data.task_data.length; iter++) {
            if ( data.task_data[iter].hasOwnProperty('rangedthumbs') ) {
                taskList = data.task_data[iter].rangedthumbs[0]
            }
        }
        taskList.map((a:any) => {
            ret.set(a.frame_no, new TaskAudio(a.fileSignName))
        })
        
        return { 'batchIdentifier' : Firebase.getUrlParameter('node_task_id'), 'taskList' :ret};
    }


    /**
     * @static 
     * Function generates the Consumer specific data format from Application Core format
     * @param param SerializedData format that is always presented from App core 
     * @returns Promise dependant on the TaskManager 
     */
    static syncTaskDataToUpstream(key: string, param: TranscribedTextSerializedData|FormInputSerializedData) :Promise<any>{
        // let obj : any= {}
        // obj.Frame_no = param.key;
        // obj.judgements = {}
        // obj.judgements.object = {}
        // obj.judgements.object.metainfo = {
        //     'image': 
        //     {
        //         'height': param.imageDetails._imageHeight, 
        //         'width': param.imageDetails._imageWidth, 
        //         'url': param.imageDetails._imageUrl
        //     }, 
        //     'annotation': 
        //     {
        //         'enabled_count': 0
        //     }
        // };
        // obj.judgements.object.annotateddata = param.data.annotations;
        // let enabledCount = param.data.annotations.filter(a => ! a.deleted && ! a.discarded).length
        // obj.judgements.object.comment = param.data.comments;
        // obj.judgements.object.metainfo.annotation.enabled_count = enabledCount;
        return this._firebaseHandle.setData(key, param)

    }

    
    /**
     * @static
     * Firebase supported taskmanager implementation for TaskData Fetching after checking Image 
     * @param id Task Sequence
     */
    static fetchTaskDataFromUpstream(id : number, audio: TaskAudio) : Promise<any> {
        return this._firebaseHandle.getData(id.toString()).catch(e => {
            AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, "GET DATA FOR ", id, e )
        }).then(res => {
                let taskData = new TaskData()
                if (res['formInput_'+id] !== undefined) {
                    taskData.comments = res['formInput_'+id];
                    delete(res['formInput_'+id])
                }
                for (let key in res) {
                    taskData.transcribedText.push(res[key]);
                }
                return new SerializedData(id, audio, taskData )
            })
    }


    static setActiveFrame(activeFrameData : ActiveFrameSerializedData) : Promise<any> {
        return this._firebaseHandle.setActiveFrame(activeFrameData.activeFrame.toString(), activeFrameData.updated_on)
    }


    static getActiveFrame() : Promise<ActiveFrameSerializedData> {
        return this._firebaseHandle.getActiveFrame().then(({activeFrame, updateTimestamp}) => {
            return new ActiveFrameSerializedData(parseInt(activeFrame),updateTimestamp)

        })
    }

    /**
     * @static
     * Helper to triger submit of whole batch
     * @returns {Promise} promise object
     */
    static saveBatch() : Promise<any> {
        return this._firebaseHandle.setSubmitStatus('submit', this.nextTaskRequest);
    }

}
