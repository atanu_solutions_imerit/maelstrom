/**
 * @file This file house TaskManager (Service) base class and a derived class for StandaloneTaskManager for TaskManager
 * which dont have any external upstream DataServer.
 */
import { MessageBus } from "../messagebus/messagebus";
import { Activity } from "../messagebus/activity";
import { Operation } from "../messagebus/operation";

import { filter } from "rxjs/operators";
import { TSMap } from "typescript-map";
import { TaskImage } from "../messagebus/taskinfo";
import { TaskAudio } from "../messagebus/taskinfo";
import { SerializedData, TaskData, ActiveFrameSerializedData, FormInputSerializedData, TranscribedTextSerializedData } from "../messagebus/taskdata";
import { AlertManager } from "../../components/alert-element/alert-element";

/**
 * @class TaskManager
 * Base class for all External Task, Task Data Provider and Generated Task Data Consumer
 */
export class TaskManager {
    /**
     * @protected @static
     * Operator set this flag to false when they dont want next Task
     */
    protected static nextTaskRequest = true;

    /**
     * @private @static
     * Function fetches the Batch details it dont contain actual Operated TaskData only.
     */
    private static fetchBatch() {
        //Pull Raw data from a URL
        this.pullRawData().then(rawdata => {
            //ActiveFrame data in first param
            this.getActiveFrame().catch(err => AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, err))
                .then((activeFrame : ActiveFrameSerializedData) => {
                    MessageBus.messageBusSubject.next(new Activity(Operation.ACTION_FETCH_BATCH, undefined, activeFrame, this.parseRawData(rawdata)));
                })
        })
    }

    /**
     * Verifies whether the ImageUrl is valid or Image itself is not corrupted
     * @param task TaskImage contains Image detail for a specific task
     * @returns Promise of TaskImage which needs N/W access/
     */
    static verifyTaskForImageLink(task : TaskImage) : Promise<TaskImage> {
        return new Promise((resolve, reject) => {
            let img = new Image()
            img.src = task._imageUrl;
            img.onload = (ev) => {
                let retTask = new TaskImage(task._imageUrl, true)
                retTask._imageHeight = img.naturalHeight;
                retTask._imageWidth =  img.naturalWidth;
                resolve(retTask);
            };
            img.onerror = (ev) => {
                let retTask = new TaskImage(task._imageUrl, false)
                resolve(retTask);
            };
        })
    }


    /**
     * @static
     * Standalone taskmanager implementation for TaskData Fetching after checking Image
     * @param ids Map between Task Sequence and Task Details
     */
    static fetchTaskData(ids : TSMap<number, TaskAudio>) {
        //console.log(ids);
        ids.forEach((a, b) => {
            //this.verifyTaskForImageLink(a).then(val => {
            //    a._imageOK = val._imageOK;
            //    a._imageHeight = val._imageHeight;
            //    a._imageWidth = val._imageWidth;
                this.fetchTaskDataFromUpstream(b!, a).then(
                    (data:any) => {
                        MessageBus.messageBusSubject.next(new Activity(Operation.ACTION_PULL_TASK_DATA_RESPONSE, undefined, b, data))
                    }
                )
            //})
        })
    }

    /**
     * @virtual
     * @protected @static
     * All Subclasses of TaskManager must implement this Function for Fetch Task Data from upstream
     * @param id Sequence number
     * @param audio TaskAudio generated
     */
    protected static fetchTaskDataFromUpstream(id : number, audio : TaskAudio) : Promise<any> {
        //update the storage for task window
        throw new Error("Subclass of Task Puller must implement fetchTaskDataFromUpstream function")

    }

    /**
     * @virtual
     * @protected @static
     * Creates the Promise to make network call for the Batch Task details
     */
    protected static pullRawData() : Promise<any> {
        throw new Error("Subclass of Task Puller must implement pullRawData function")
    }

    /**
     * @virtual
     * @protected @static
     * Function parses the raw Data pulled from network
     * @param data Raw Data needs parsing
     */
    protected static parseRawData(data:any) : {batchIdentifier: string, taskList : TSMap<number, TaskAudio>} {
        throw new Error("Subclass of Task Puller must implement parseRawData function")
    }

    /**
     * @static
     * Function generates the Consumer specific data format from Application Core format
     * @param param SerializedData format that is always presented from App core
     * @returns Promise defining success/failure while syncing
     */
    static syncTaskDataToUpstream(key: string, param: TranscribedTextSerializedData|FormInputSerializedData) : Promise<any> {
        throw new Error("Subclass of Task Puller must implement syncTaskDataToUpstream function")
    }

    /**
     * @static
     * Helper to land on the last operated frame in this task
     * @param frameno index in the batch of task
     */
    static setActiveFrame(activeFrameData : ActiveFrameSerializedData) : Promise<any>{
        throw new Error("Subclass of Task Pull must implement setActiveFrame function");
    }


    /**
     * @static
     * Helper to land on the last operated frame in this task
     * @param frameno index in the batch of task
     */
    static getActiveFrame() : Promise<ActiveFrameSerializedData> {
        throw new Error("Subclass of Task Pull must implement setActiveFrame function");
    }

    /**
     * @static
     * Helper to triger submit of whole batch
     * @returns {Promise} promise object
     */
    static saveBatch() : Promise<any> {
        throw new Error("Subclass of Task Pull must implement saveBatch function");
    }


    /**
     * @static
     * Register all global MessageBus specific Event Listener and also kickstarts the Batch Fetching
     */
    static start() {
        MessageBus.messageBusObservables
            .pipe(filter(a => a.action === Operation.ACTION_PULL_TASK_DATA ))
            .subscribe(a => this.fetchTaskData(a.poststate))

        MessageBus.messageBusObservables
            .pipe(filter(a => a.action === Operation.ACTION_PUSH_TASK_DATA ))
            .subscribe(a => {
                this.syncTaskDataToUpstream(a.prestate, a.poststate).then(() => {
                    MessageBus.messageBusSubject.next(new Activity(Operation.ACTION_PUSH_TASK_DATA_SUCCESS))
                }).catch(() => {
                    MessageBus.messageBusSubject.next(new Activity(Operation.ACTION_PUSH_TASK_DATA_FAILURE))
                })
            })

        MessageBus.messageBusObservables
            .pipe(filter(a => a.action === Operation.ACTION_LOAD_TASK_REQUEST ))
            .subscribe(a => {
                this.setActiveFrame(new ActiveFrameSerializedData(a.prestate))
            })

        MessageBus.messageBusObservables
            .pipe(filter(a => a.action === Operation.TASK_SUBMIT ))
            .subscribe(a => {
                this.saveBatch();
            })

        MessageBus.messageBusObservables
            .pipe(filter(a => a.action === Operation.END_OF_TASK_REQUESTED ))
            .subscribe(a => {
                this.nextTaskRequest = !this.nextTaskRequest;
            })

        this.fetchBatch();
    }


}

/**
 * @class @extends TaskManager
 * Standalone Implementation version dont have upstream Task Provider and TaskData Consumer
 */
export class StandaloneTaskManager extends TaskManager {

    /**
     * @static
     * Return the Promise for StandaloneTaskManager its a dummy because there is no upstream Task Provider
     * @returns Promise with nothing as value
     */
    static pullRawData() : Promise<any> {
        return new Promise((resolve, reject) => resolve({}));
    }



    /**
     * @static
     * StandAlone TaskManager generates dummy Task Set data
     * @param data Unused Param for Standalone TaskManager
     */
    // static parseRawData(data: any) : {batchIdentifier: string, taskList : TSMap<number, TaskImage>}{
    //     let ret = new TSMap<number, TaskImage>();
    //     ret.set(1, new TaskImage("https://s3.us-east-2.amazonaws.com/insignia-client/img10.jpg"))
    //     ret.set(2, new TaskImage("https://s3.us-east-2.amazonaws.com/insignia-client/img11.jpg"))
    //     ret.set(3, new TaskImage("https://s3.us-east-2.amazonaws.com/insignia-client/img12.jpg"))
    //     ret.set(4, new TaskImage("https://s3-ap-southeast-1.amazonaws.com/imerit-solution/client/Percipient/raw_images%2FData/00005.jpg"))
    //     ret.set(5, new TaskImage("https://s3.us-east-2.amazonaws.com/insignia-client/img20.jpg"))
    //     ret.set(6, new TaskImage("https://s3.us-east-2.amazonaws.com/insignia-client/img10.jpg"))
    //     ret.set(7, new TaskImage("https://s3.us-east-2.amazonaws.com/insignia-client/img11.jpg"))
    //     ret.set(8, new TaskImage("https://s3.us-east-2.amazonaws.com/insignia-client/img12.jpg"))
    //     ret.set(9, new TaskImage("https://s3-ap-southeast-1.amazonaws.com/imerit-solution/client/SmartAg/2018/10-19%2Fimages/image_1539887171567.0156.jpeg"))
    //     ret.set(10, new TaskImage("https://s3.us-east-2.amazonaws.com/insignia-client/img10.jpg"))
    //     ret.set(11, new TaskImage("https://s3.us-east-2.amazonaws.com/insignia-client/img11.jpg"))
    //     ret.set(12, new TaskImage("https://s3.us-east-2.amazonaws.com/insignia-client/img12.jpg"))
    //     ret.set(13, new TaskImage("https://s3-ap-southeast-1.amazonaws.com/imerit-solution/client/SmartAg/2018/10-19%2Fimages/image_1539891964959.3298.jpeg"))
    //     ret.set(14, new TaskImage("https://s3.us-east-2.amazonaws.com/insignia-client/img10.jpg"))
    //     ret.set(15, new TaskImage("https://s3.us-east-2.amazonaws.com/insignia-client/img11.jpg"))
    //     ret.set(16, new TaskImage("https://s3.us-east-2.amazonaws.com/insignia-client/img12.jpg"))

    //     return { 'batchIdentifier' : "DummyText", 'taskList' :ret};
    // }

    static parseRawData(data: any) : {batchIdentifier: string, taskList : TSMap<number, TaskAudio>}{
        let ret = new TSMap<number, TaskAudio>();
        ret.set(1, new TaskAudio("https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3"))
        //ret.set(1, new TaskAudio("http://192.168.2.245/maelstorm_audio_files/oggtest.ogg"))
        ret.set(2, new TaskAudio("http://192.168.2.245/maelstorm_audio_files/couchplayin.wav"))


        return { 'batchIdentifier' : "DummyText", 'taskList' :ret};
    }


    /**
     * @static
     * Function generates the Consumer specific data format from Application Core format
     * @param param SerializedData format that is always presented from App core
     * @returns Promise to define succesful/failure while syncing
     */
    static syncTaskDataToUpstream(key: string, param: TranscribedTextSerializedData|FormInputSerializedData) : Promise<any> {
        return Promise.resolve()
    }


    static reverseSyncTaskDataToUpstream(data : any) {
        let retTaskData = new TaskData()
        let retAudioData = new TaskAudio(data.judgements.object.metainfo.audio.url, false)
        retTaskData.comments = data.judgements.object.comment;
        retTaskData.transcribedText = data.judgements.object.annotateddata;
        //retImageData._imageHeight = data.judgements.object.metainfo.image.height;
        //retImageData._imageWidth = data.judgements.object.metainfo.image.width;

        return new SerializedData(data.Frame_no, retAudioData, retTaskData)
    }

    /**
     * @static

     * Standalone taskmanager implementation for TaskData Fetching after checking Image
     * @param id Task Sequence
     * @param audio
     */
    static fetchTaskDataFromUpstream(id : number, audio: TaskAudio) : Promise<any> {
        //SerializedData to TaskData conversion
        let tData = new TaskData();
        let taskObj = tData.transcribedText;
        let tsdata :any= {};

        tsdata.id = "_abcd";//: string,
        tsdata.text = "Testing"; //: string,
        tsdata.editable = false;//: boolean,
        tsdata.updated_on = Date.now()//: number,
        tsdata.start_time = Date.now() - 300;//: number,
        tsdata.end_time = Date.now() - 250;//: number,
        tsdata.speaker = "Sujit";//: string,
        tsdata.gid = "OP";//: string,
        tsdata.seqno = 2 //: number
        taskObj.push(tsdata as TranscribedTextSerializedData);

        let tsdata1 :any= {}

        tsdata1.id = "_abcdef";//: string,
        tsdata1.text = "Testing 11"; //: string,
        tsdata1.editable = false;//: boolean,
        tsdata1.updated_on = Date.now()//: number,
        tsdata1.start_time = Date.now() - 300;//: number,
        tsdata1.end_time = Date.now() - 250;//: number,
        tsdata1.speaker = "Suman";//: string,
        tsdata1.gid = "OP";//: string,
        tsdata1.seqno = 1 //: number
        taskObj.push(tsdata1 as TranscribedTextSerializedData);

        let tsdata2 :any= {}

        tsdata2.id = "_11abcd";//: string,
        tsdata2.text = "Testing 22"; //: string,
        tsdata2.editable = false;//: boolean,
        tsdata2.updated_on = Date.now()//: number,
        tsdata2.start_time = Date.now() - 300;//: number,
        tsdata2.end_time = Date.now() - 250;//: number,
        tsdata2.speaker = "Sujit";//: string,
        tsdata2.gid = "QC";//: string,
        tsdata2.seqno = 2 //: number
        taskObj.push(tsdata2 as TranscribedTextSerializedData);

        let tsdata3 :any= {}

        tsdata3.id = "_11abcdef";//: string,
        tsdata3.text = "Testing 33"; //: string,
        tsdata3.editable = false;//: boolean,
        tsdata3.updated_on = Date.now()//: number,
        tsdata3.start_time = Date.now() - 300;//: number,
        tsdata3.end_time = Date.now() - 250;//: number,
        tsdata3.speaker = "Suman";//: string,
        tsdata3.gid = "QC";//: string,
        tsdata3.seqno = 1 //: number



        //tData.transcribedText = new Array<TranscribedTextSerializedData>();
        taskObj.push(tsdata3 as TranscribedTextSerializedData);
        return  Promise.resolve(new SerializedData(id, audio, tData ));
    }


    static setActiveFrame(activeFrameData : ActiveFrameSerializedData) : Promise<any> {
        return Promise.resolve("")
    }


    static getActiveFrame() : Promise<ActiveFrameSerializedData> {
        return Promise.resolve(new ActiveFrameSerializedData(1));
    }

    /**
     * @static
     * Helper to triger submit of whole batch
     * @returns {Promise} promise object
     */
    static saveBatch() : Promise<any> {
        return Promise.resolve({success:true});
    }
}
