import {Subject, Observable} from 'rxjs'
import {Activity} from './activity'



/**
 * Global Message Bus implementation with RxJS Subject which can acts both as Observable as well as Generator of Events (Activity)
 */
export class MessageBus {
    /**
     * @member messageBusSubject Event Generator Interface
     */
    public static messageBusSubject : Subject<Activity> = new Subject<Activity>(); 
    /**
     * @member messageBusObservables Event Listener Interface
     */
    public static messageBusObservables : Observable<Activity> = MessageBus.messageBusSubject.asObservable();
}

