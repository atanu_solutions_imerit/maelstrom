import {Operation} from './operation'
/**
 * @interface UndoRedoAble Interface needs to be implemented by those entities which wants to be stored in Activity Ledger for Undo Redo Operation
 */
export interface UndoRedoAble {
    undoAction(action: Operation, state: any): void;
    redoAction(action: Operation, state: any): void;
}