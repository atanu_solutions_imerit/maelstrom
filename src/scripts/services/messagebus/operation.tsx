export enum Operation {

    /** Fetch for a single annotation done */
    ACTION_FETCH,
    /** Fetching of Comments for requested frame complete */
    ACTION_FETCH_COMMENT,
    /** Fetch of all annotation done*/
    ACTION_FETCH_COMPLETE,
    /** Request to TaskManagers to pull Batch details which include TaskDetails only at bootstrap */
    ACTION_FETCH_BATCH,

    /** Pulling Tasking Data (List of Annotations) for a single Task */
    ACTION_PULL_TASK_DATA,
    /** Pushing Task Data of a single task to upstream Consumer */
    ACTION_PUSH_TASK_DATA,
    /** Pull Tasking Data Response (List of Annotations) for a single Task */
    ACTION_PULL_TASK_DATA_RESPONSE,
    /** Request to initiate Switching of Frame Operation  */
    ACTION_LOAD_TASK_REQUEST, //32

    /** Request to Fetch Config for this batch */
    ACTION_CONFIG_REQUEST,
    /* Action denotes that Config has been fetched by helper task modules */
    ACTION_CONFIG_FETCHED,
    /* Config Manager invalidated the config downloaded by helper task modules */
    ACTION_CONFIG_INVALIDATED,

    /** Fit to Screen Tool Controls Click Event */
    FIT_TO_SCREEN_KEYED,
    /** Deselect all Selected Annotation Controls Click Event */
    DESELECT_ALL_KEYED, //54
    /** Brightness and Contrast Reset Press Event */
    RESET_HUE_KEYED,
    /** Copy Selected Annotation to clipboard */
    COPY_SELECTED_KEYED,
    /** Copy all Annotation from present frame to clipboard */
    COPY_ALL_KEYED,
    /** Paste from clipboard to present frame  */
    PASTE_KEYED,
    /** Flush all annotation details from clipboard */
    CLEAR_CLIPBOARD_KEYED,
    /** Displace all selected annotation to right */
    DISPLACE_RIGHT,
    /** Displace all selected annotation to left */
    DISPLACE_LEFT,
    /** Displace all selected annotation upwards */
    DISPLACE_UP,
    /** Displace all selected annotation downwards */
    DISPLACE_DOWN,

    /** Navigation to previous frame in batch requested */
    PREV_NAV_CLICK,
    /** Navigation to previous frame allowedd */
    PREV_NAV_ALLOWED,
    /** Navigation to next frame in batch requested */
    NEXT_NAV_CLICK,
    /** Navigation to next frame allowed */
    NEXT_NAV_ALLOWED,
    /** Goto Specific Index in batch*/
    GOTO_NAV_CLICK,
    /** Goto Specific Index allowed*/
    GOTO_NAV_ALLOWED,
    /** Selectable or Editable Comment change event */
    COMMENT_CHANGE,
    /** Operator initiated Draw End Action for Polygon and Polyline */
    DRAW_END_REQUEST,
    /** Show all labels at one go */
    SHOW_ALL_LABEL,
    /** Hide all labels at one go */
    HIDE_ALL_LABEL,
    /** Show all annotation at one go */
    SHOW_ALL_ANNOTATION,
    /** Hide all annotation at one go */
    HIDE_ALL_ANNOTATION,
    /** Show grid on image */
    SHOW_GRID,
    /** Hide grid on image */
    HIDE_GRID,
    /** Change Brightness level */
    CHANGE_BRIGHTNESS,
    /** Change Contrast level */
    CHANGE_CONTRAST,
    /** On Submit button click */
    TASK_SUBMIT_REQUEST,
    /**Final Submit Request Allowed */
    TASK_SUBMIT_ALLOWED,
    /**Final Submit for dirty frame conditional */
    TASK_SUBMIT,
    /** Successful response from TaskManager for syncing  */
    ACTION_PUSH_TASK_DATA_SUCCESS,
    /** Failure response from TaskManager for syncing  */
    ACTION_PUSH_TASK_DATA_FAILURE,
    /** Image Segment Manager control clicked */
    SEGMENT_CLICKED,
    /** Viewbox change event */
    VIEWBOX_CHANGE,
    /** Time gets updated */
    TIME_UPDATE,
    /** Event denoting Image Segments calculated */
    SEGMENT_CALCULATED,
    /** Browser unload Event */
    PAGE_UNLOAD,
    /** Request by operator for no more tasking */
    END_OF_TASK_REQUESTED,
    /* Last Volume level*/
    VOLUME_LEVEL,
    /*Play Back change*/
    PLAYBACK_CHANGE,
    /*start time of audio*/
    TIMELINEDATASTART,
    /*start time of audio*/
    TIMELINEDATAEND,

    EXIT_FINALISED

}
