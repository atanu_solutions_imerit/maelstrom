import {UndoRedoAble} from './undoredoable'
import {Operation} from './operation'

/**
 * Class denotes the Activity which is the globally agreed message format in MessageBus. All action that needs to be advertised should be
 * broadcasted with a new Activity over RxObservable channel.
 */
export class Activity {
    /** Any Object which will implement UndoRedoAble interface will be eligible for this because during Undo Redo Phase this same object will be used */
    object : UndoRedoAble|undefined;
    /** Globally unique name is provided for each operation */
    action : Operation;
    /** Data Field one/Prestate before the operation has actually executed */
    prestate : any;
    /** Data Field two/Poststate after the operation has been executed */
    poststate : any;
    /** Flag to denotes that this Activity is eligible for storage in ActivityLedger for future undoredo operation */
    undoredoable : boolean;

    /**
     * @constructor
     * @param action Enum values of Operation
     * @param object UndoRedoAble Instances
     * @param prestate Data Field one
     * @param poststate Data Field two
     * @param undoredoable Eligible for putting into Activity ledger
     * @returns Activity Object which will be used in Inter modular messaging
     */
    constructor(action: Operation, object: UndoRedoAble|undefined = undefined, prestate: any = null, poststate : any = null, undoredoable : boolean = true) {
        this.object = object;
        this.action = action;
        this.prestate = prestate;
        this.poststate = poststate;
        this.undoredoable = undoredoable;
    }
}
