/**
 * @file This file houses two class both for TaskInformation
 */

/**
 * @class
 * Image Releated information required inside TaskInfo
 */
export class TaskImage {
    _imageUrl : string;
    _imageOK : boolean;
    _imageHeight : number;
    _imageWidth : number;

    constructor(image: string, ok:boolean = true) {
        this._imageUrl = image;
        this._imageOK = ok;
        this._imageHeight = 0;
        this._imageWidth = 0;
    }
}


export class TaskAudio {
    _audioUrl : string;
    constructor(audio: string, ok:boolean = true) {
        this._audioUrl = audio;
    }
}
/**
 * @class
 * TaskInfo is the datatype for all TaskInformation that upstream server will provide
 */

export class TaskInfo {
    identifier : string;
    audio : TaskAudio;
    batchSize : number;
    batchPosition : number;

    constructor(identifier:string, audioUrl:TaskAudio, batchSize:number, batchPosition:number) {
        this.identifier = identifier;
        this.audio = audioUrl;
        this.batchPosition = batchPosition;
        this.batchSize = batchSize;
    }
}
