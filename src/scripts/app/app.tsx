/**
 * @file This File house the EntryPoint of this application viz, App
 */
import 'jquery';
import 'bootstrap/dist/css/bootstrap.css';
import '../../styles/zoomindependant.css';
import 'mdbootstrap/css/mdb.css';
import appStyle from './app.module.css';
import cx from 'classnames';
import 'bootstrap';
import '../../img/NoImageFound.png';
import '../../img/done.png';
import { filter} from 'rxjs/operators/';
import {myOwnJSX} from '../utils/common/jsxFactory';
import { MessageBus } from '../services/messagebus/messagebus';
import { Operation } from '../services/messagebus/operation';
import { TaskWindow } from '../services/taskwindow/taskwindow';
import { ConfigManager } from '../services/configservice/configmanager';
import { FormPanelState } from '../views/formpanel/formpanel_state';
import { StandaloneTaskManager } from '../services/taskservice/taskmanager';
import { AlertManager } from '../components/alert-element/alert-element';
import { FirebaseTaskManager } from '../services/taskservice/firebasetaskmanager';
import { LoaderManager } from '../components/loader-element/loader-element';
import { TranscriptionManager } from '../services/transcriptionmanager/transcriptionmanager';
import { MediaState } from '../views/media/media_state';

/**
 * @class
 * EntryPoint of the whole App
 */
export class App {
    /**
     * @private
     * SingleTon instance of this application
     */
    private static _instance: App;

    /**
     * @constructor
     * Renders the TopLevel UI Components of this application as well as creates AnnotationCollectionState
     * which internally manages the View related to AnnotationCollection
     */
    constructor() {
        if (App._instance === undefined) {
            this.viewInit()
            App._instance = this;
        }
        return App._instance;
    }

    /**
     * @public
     * ConfigManager is the First module to be kickstarted, succesful fetching of which paves the way for other modules
     * to start.
     */
    initiateHelperComponent() {
        MessageBus.messageBusObservables
        .pipe(filter(a => a.action === Operation.ACTION_CONFIG_FETCHED))
        .subscribe(a => {
            TaskWindow.start();
            try {
                if (process.env.APP_TASK_MANAGER === 'StandaloneTaskManager') {
                    StandaloneTaskManager.start()
                } else if (process.env.APP_TASK_MANAGER === 'FirebaseTaskManager') {
                     FirebaseTaskManager.start()
                 } else {
                     AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, "No Valid TaskManager associated with this build")
                     return;
                 }
            } catch(error) {
                AlertManager.processMsg(AlertManager.NOTIFICATION_TYPE.ERROR_NOTIFICATION, "Failure in initializing TaskManager");
                return;
            }
            MediaState.start(App.MAINDIV);
            TranscriptionManager.start(App.MAINDIV);
            FormPanelState.start(App.FORM_PANEL);
        });

        MessageBus.messageBusObservables
        .subscribe(a => {
            document.getElementById(App.MAINDIV)!.focus();
        });

        ConfigManager.start();
    }

    /**
     * @private
     * Initializes the View with top components
     */
    private viewInit() {
        let documentView = (
            <div style="height: 100vh">
                <div class="flex-center flex-column" id={App.INTRO} style="display: none">
                    <h1 class="animated fade-in mb-2">Maelstorm</h1>
                    <h5 class="animated fade-in mb-1">Audio Transcription Tool</h5>
                    <button class="btn btn-primary" id={App.PLAYSTART}>Load Playground</button>
                </div>

                <div id={App.PLAYDIV}>
                    <div class="container-fluid">
                        <div class="row">
                            <div class={cx('col-sm-12', appStyle.calculatedFix, appStyle.noPad)}>
                                <div tabindex="-1" id={App.MAINDIV} class={cx(appStyle.drawCanvas)}/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div id={App.FORM_PANEL}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
        document.body.appendChild(documentView);
        LoaderManager.processLoading(LoaderManager.LOADING_TYPE.PLAIN_NOTIFICATION, null, 'show');
    }
}


export namespace App {
    /** Div for Audio and Text Editor  */
    export const MAINDIV = 'main';
    /** Div for Lower Panel which contains all transition and Image Details and Comments */
    export const FORM_PANEL = 'form-panel';
    /** Working screen */
    export const PLAYDIV = 'play';

    /** Intro screen  */
    export const INTRO = 'intro';
    /** UI Button to switch from Intro screen to Working screen */
    export const PLAYSTART = 'startTool';
}

const lightning2App = new App();
lightning2App.initiateHelperComponent();


