var Ajv = require('ajv');

/**
 * Utility Class which validates a JSON against a JSON schema using
 * AJV
 */
export class JSONValidator {
    /**
     * API to call AJV for JSON validation
     * @static
     * @param schema Against which Schema is validated
     * @param data JSON object that needs to be validated
     * @returns true/false
     */
    static validateJSONSchema(schema : Object, data : Object) : boolean {
        const validator = new Ajv({schemaId: 'auto'})
        return validator.validate(schema, data)
    }
}
