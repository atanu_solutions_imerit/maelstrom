
/**
 * This class will be used to call generic helper functions
 */
export class Utils {
  static isColinear(p1: Vertex, p2: Vertex, p3: Vertex) {
    let d1 = Math.sqrt(Math.pow(p1.x - p3.x, 2) + Math.pow(p1.y - p3.y, 2));
    let d2 = Math.sqrt(Math.pow(p2.x - p3.x, 2) + Math.pow(p2.y - p3.y, 2));
    let d3 = Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2));
    // Formulae of area of triangle i/2 { (x1-x2)*(y2-y3) - (y1-y2)(x2-x3) }
    let areatwice =
      (p1.x - p2.x) * (p2.y - p3.y) - (p1.y - p2.y) * (p2.x - p3.x);
    let altitude = Math.abs(areatwice) / d3;

    if (altitude < 1 && Math.abs(d3 - d1 - d2) < 1) return true;
    else return false;
  }
}

export type Vertex = {
  x : number,
  y : number
}
