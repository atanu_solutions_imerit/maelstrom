/**
 * @file This file house ViewState class viz, FormPanelState that contains all User
 * Text and Selectable Input for a single Task.
 * Second Class is a FormInput which contains logic to serialize and deserialize
 */
import { filter } from 'rxjs/operators';
import { Operation } from '../../services/messagebus/operation';
import { MessageBus } from '../../services/messagebus/messagebus';
import { Activity } from '../../services/messagebus/activity';
import { FormPanel } from './formpanel';
import { TSMap } from 'typescript-map';
import { TaskInfo, TaskImage, TaskAudio } from '../../services/messagebus/taskinfo';
import { FormInputSerializedData } from '../../services/messagebus/taskdata';

/**
 * @class
 * Class manages the Text and Selectable input for a single Task
 */
export class FormPanelState {
    /**
     * @static @private
     * FormInput handles which contains user input
     */
    private static _fInput: FormInput;
    /**
     * End of Task status
     */
    static nextTaskRequest : boolean = false;


    /**
     * @static
     * Renders the View, registers its callback with View and Event Listener for global MessageBus
     * @param parentDiv HTMLDivElement on which all the View element will be rendered
     */
    static start(parentDiv: string) {
        this._fInput = new FormInput()
        FormPanel.renderView(parentDiv)
        FormPanel.start(this.callbackHandler.bind(this));
        MessageBus.messageBusObservables
            .pipe(filter(a => a.action === Operation.ACTION_FETCH_COMMENT))
            .subscribe(a => {
                this._fInput.serializedData = a.poststate;
                FormPanel.updateComment(this._fInput)
            })

        MessageBus.messageBusObservables
            .pipe(filter(a => a.action === Operation.ACTION_LOAD_TASK_REQUEST))
            .subscribe(a => {
                FormPanel.updateTaskInfo(a.poststate as TaskAudio);
                this._fInput.imageHeight = (a.poststate as TaskImage)._imageHeight;
                this._fInput.imageWidth = (a.poststate as TaskImage)._imageWidth;
                this._fInput.imageURL = (a.poststate as TaskImage)._imageUrl;
            });

        MessageBus.messageBusObservables
            .pipe(filter(a => a.action === Operation.ACTION_FETCH_COMPLETE))
            .subscribe(a => {
                FormPanel.updateBatchInfo(a.poststate as TaskInfo)
            })

        MessageBus.messageBusObservables
            .pipe(filter(a => a.action === Operation.VIEWBOX_CHANGE))
            .subscribe(a => {
                //FormPanel.updateViewBox(a.poststate)
            })

        MessageBus.messageBusObservables
            .pipe(filter(a => a.action === Operation.PAGE_UNLOAD))
            .subscribe(a => {
                let poststate = Object.assign(Object.create(this._fInput.constructor.prototype), this._fInput);;
                MessageBus.messageBusSubject.next(new Activity(Operation.COMMENT_CHANGE, undefined, {}, poststate))
            })
    }

    /**
     * @static
     * Function is the callback that needs to be registered with View.
     * @param type Set of Constant defining action taken by View
     * @param params Parameter required for that action
     */
    static callbackHandler(type: Operation, ...params: Array<any>) {
        let prestate : any;
        let poststate : any;
        let obj : any;
        switch (type) {
            case Operation.NEXT_NAV_CLICK:
                poststate = Object.assign(Object.create(this._fInput.constructor.prototype), this._fInput);
                MessageBus.messageBusSubject.next(new Activity(Operation.COMMENT_CHANGE, undefined, prestate, poststate))
                MessageBus.messageBusSubject.next(new Activity(Operation.NEXT_NAV_CLICK))
                break;
            case Operation.PREV_NAV_CLICK:
                poststate = Object.assign(Object.create(this._fInput.constructor.prototype), this._fInput);
                MessageBus.messageBusSubject.next(new Activity(Operation.COMMENT_CHANGE, undefined, prestate, poststate))
                MessageBus.messageBusSubject.next(new Activity(Operation.PREV_NAV_CLICK))
                break;
            case Operation.COMMENT_CHANGE:
                prestate = Object.assign(Object.create(this._fInput.constructor.prototype), this._fInput);
                obj = params[0]
                for (let key in obj) {
                    if (key === 'comment') {
                        this._fInput.textComment = obj[key].trim();
                    } else {
                        this._fInput.predefinedComment.set(key, obj[key])
                    }
                }
                poststate = Object.assign(Object.create(this._fInput.constructor.prototype), this._fInput);
                MessageBus.messageBusSubject.next(new Activity(Operation.COMMENT_CHANGE, undefined, prestate, poststate))
                break;
            case Operation.TASK_SUBMIT_REQUEST:
                poststate = Object.assign(Object.create(this._fInput.constructor.prototype), this._fInput);
                MessageBus.messageBusSubject.next(new Activity(Operation.COMMENT_CHANGE, undefined, prestate, poststate))
                MessageBus.messageBusSubject.next(new Activity(Operation.TASK_SUBMIT_REQUEST))
                break;
            case Operation.SEGMENT_CLICKED:
                MessageBus.messageBusSubject.next(new Activity(Operation.SEGMENT_CLICKED, undefined, null, params[0]))
                prestate= Object.assign(Object.create(this._fInput.constructor.prototype), this._fInput);
                this._fInput.imagePartition = params[1]
                poststate = Object.assign(Object.create(this._fInput.constructor.prototype), this._fInput);
                MessageBus.messageBusSubject.next(new Activity(Operation.COMMENT_CHANGE, undefined, prestate, poststate))
                break;
            case Operation.TIME_UPDATE:
                this._fInput.timeSpent = params[0];
                break;
            case Operation.SEGMENT_CALCULATED:
                MessageBus.messageBusSubject.next(new Activity(Operation.SEGMENT_CALCULATED, undefined, params[0], params[1]))
                break;
            case Operation.END_OF_TASK_REQUESTED:
                MessageBus.messageBusSubject.next(new Activity(Operation.END_OF_TASK_REQUESTED))
                break;
        }
    }
}

/**
 * @class FormInput
 * Text and Selectable Comment Holder
 */
export class FormInput {
    /**
     * TextComment for this Task
     */
    textComment: string = "";
    /**
     * Map of Selectable Item Name and Value(s) selected by user
     */
    predefinedComment = new TSMap<string, string[]>();
    /**
     * Image Partition details
     */
    imagePartition ?: Array<boolean>;
    /**
     * Time spent on this Task
     */
    timeSpent : number = 0;
    /**
     * Image Width
     */
    imageWidth : number = 0;
    /**
     * Image Height
     */
    imageHeight : number = 0;
    /**
     * Image URL
     */
    imageURL : string = '';
    /**
     * Image Category
     */
    imageCategory : string = '';


    /**
     * @property
     * Generates Serialized State of this object
     * @return FormInputSerializedData
     */
    get serializedData(): FormInputSerializedData {
        let tobj: any = {}
        this.predefinedComment.forEach((value, key) => {
            tobj[key!] = value;
        })
        let data: FormInputSerializedData = new FormInputSerializedData()
        data.textComment = this.textComment;
        data.predefinedComment = tobj;
        //data.imagePartition = this.imagePartition || [];
        data.timeSpent = this.timeSpent;
        //data.imageWidth = this.imageWidth;
        //data.imageHeight = this.imageHeight;
        //data.imageURL = this.imageURL;
        //data.imageCategory = this.imageCategory;
        data.updated_on = Date.now()
        return data
    }

    /**
     * @property
     * Creates Object state from Serialized Version
     * @param object Serialized Data that will regenerated the Object state
     */
    set serializedData(object: FormInputSerializedData) {
        let data: any = object;
        this.textComment = object.textComment ? object.textComment : "";
        //this.imagePartition = object.imagePartition || [];
        this.timeSpent = object.timeSpent || 0;
        this.predefinedComment = new TSMap<string, string[]>();
        // if (object.imageWidth !== undefined && object.imageWidth !== 0) {
        //     this.imageWidth = object.imageWidth;
        //     this.imageHeight = object.imageHeight;
        //     this.imageURL = object.imageURL;
        //     this.imageCategory = object.imageCategory;
        // }
        for (let key in object.predefinedComment) {
            this.predefinedComment.set(key, object.predefinedComment[key])
        }
    }

}
