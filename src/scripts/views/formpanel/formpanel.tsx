/**
 * @file View for the FormPanel is the primary class. There are several helper View Elements viz,
 * Information, TextComment, SelectableComment, PrevNavControl, PrevNextControl, NavInfo
 */
import {fromEvent, Subscription, timer} from 'rxjs'
import {myOwnJSX} from '../../utils/common/jsxFactory'
import { filter, tap } from 'rxjs/operators';
import { Operation } from '../../services/messagebus/operation';
import mdbStyles from '../../../styles/mdb.module.css';
import formStyle from './formpanel.module.css';
import cx from 'classnames';
import { FormInput } from './formpanel_state';
import { ConfigManager } from '../../services/configservice/configmanager';
import { TaskInfo, TaskAudio } from '../../services/messagebus/taskinfo';
import '../../../external/multiple-select.css';
import SlimSelect from 'slim-select';
import { Option } from 'slim-select/dist/data';
import { Config } from "../../services/configservice/config";

/**
 * @class
 * FormPanel contains View which covers the footer part of the Application
 */
export class FormPanel {
    /**
     * @static
     * FormPanel gets enabled when images gets landed after switching
     */
    static formEnabled = true;

    /**
     * @static
     * Initiates Dom Element creaation for this View
     * @param parentDiv HTMLDivElement where this View will render
     */

    static renderView(parentDiv: string) {

        $('#'+parentDiv).append(
            <div class={cx(formStyle.form__class, 'row')} style="height: 47px;">
                <div class={cx('col-sm-2', 'p-2', formStyle.formplacement, formStyle.brdr)}>
                    <div class={cx('row')}>
                        <div class={cx('col-sm-3')}>
                            {Information.getUIComponent()}
                        </div>
                        <div class={cx('col-sm-9')}>
                            <nav aria-label="Page navigation example">
                                <ul class={cx(formStyle.pagination, 'justify-content-center')} style="margin-bottom: 0;">
                                    {PrevNavControl.getUIComponent()}
                                    {NavInfo.getUIComponent()}
                                    {NextNavControl.getUIComponent()}
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class={cx('col-sm-2', formStyle.formplacement, formStyle.brdr)} style="padding: 2px;">
                    {TextComment.getUIComponent()}
                </div>
                <div class={cx('col-sm-7', formStyle.formplacement)} style="padding-right: 0px;">
                    <div class="row" style="padding:0px 15px;">
                        {SelectableComment.getUIComponent()}
                    </div>
                </div>
                <div class={cx('col-sm-1', formStyle.formplacement)} style="padding-right: 0px;">
                    {SubmitComponent.getUIComponent()}
                </div>
                {Information.getPopupUIComponent()}
                {SelectableComment.getPopupUIComponent()}
            </div>
        )
    }

    /**
     * @static
     * Starts registering Views Callback from controlling ViewState
     * @param callback ViewState Callback on which this View will inform about activity
     */
    static start(callback: Function) {
        PrevNavControl.getObservables(callback)
        NextNavControl.getObservables(callback)
        TextComment.getObservables(callback)
        SelectableComment.getObservables(callback)
        Information.getObservables(callback)
        SubmitComponent.getObservables(callback)
    }

    /**
     * @static
     * Updates BatchInformation in Navigation Bar
     * @param data BatchInfo which provide info to be updated
     */
    static updateBatchInfo(data: TaskInfo) {
        this.formEnabled = true;
        NavInfo.updateData(data);
        PrevNavControl.updateData(data);
        NextNavControl.updateData(data);
        Information.updateBatchView(data)
        SubmitComponent.updateView(data);
    }

    /**
     * @static
     * Updates TaskInformation in Navigation Bar
     * @param data TaskInfo which provide info to be updated
     */
    static updateTaskInfo(data: TaskAudio) {
        Information.updateTaskView(data)
    }

    /**
     * @static
     * Updates Text and Selectable Comment in FormPanel View
     * @param data FormInput which contains all user provided input
     */
    static updateComment(data:FormInput) {
        let config :any = ConfigManager.getConfig()
        TextComment.updateView(data.textComment)
        $('#'+FormPanel.FORM_ELEMENT.SELECTABLE_DIV).empty()
        SelectableComment.clearOptions();
        let totalCount = config.selectableComment.length + config.multiSelectableComment.length;
        config.selectableComment.forEach((a:Config.ISelectableComment) => {
            SelectableComment.generateOption(a.name, a.dataPlaceholder, a.default, a.options, data.predefinedComment.get(a.name), totalCount, false)
        })
        config.multiSelectableComment.forEach((a:Config.IMultiSelectableComment) => {
            SelectableComment.generateOption(a.name, a.dataPlaceholder, a.default, a.options, data.predefinedComment.get(a.name), totalCount, true)
        })
        Information.updateTime(data.timeSpent);

    }
}

/**
 * @class
 * Information View Component
 */
export class Information {
    /**
     * Interval subscriptin
     */
    static time : number;

    /**
     * @static
     * Renders View
     */
    static getUIComponent() {
        return (
            <div class={cx(formStyle.formplacement, formStyle.brdr)}>
                <a id={FormPanel.FORM_ELEMENT.TASK_INFO_DIV} class={cx('page-link', formStyle.paginationPageLinks, 'primary-color')} href="#" style="float: right; height: 32px; width: 32px; margin-left: 25px;">
                    <i class="material-icons">list</i>
                </a>
            </div>

        )
    }

    /**
     * @static
     * Renders PopupBlock view
     */
    static getPopupUIComponent() {
        return (
            <div class={cx(formStyle.taskInfoPop, mdbStyles.card)} id={FormPanel.FORM_ELEMENT.POPBLOCK_DIV} style="display: none;">
                <span class={cx(formStyle.tasktitle)}>Task Information</span>
                <ul class={cx(mdbStyles.listGroup, formStyle.formList)}>
                <li  class={cx(mdbStyles.listGroupItem, formStyle.formStnpad, 'd-flex', 'justify-content-between', 'align-items-center')}>
                    <span id={FormPanel.FORM_ELEMENT.TASK_ID} class={formStyle.longText}></span>
                    <span class={cx(mdbStyles.badge, mdbStyles.badgePrimary, mdbStyles.badgePill)}>TASK ID</span>
                </li>
                <li  class={cx(mdbStyles.listGroupItem, formStyle.formStnpad, 'd-flex', 'justify-content-between', 'align-items-center')}>
                    <span id={FormPanel.FORM_ELEMENT.IMAGE_NAME} class={formStyle.longText}></span><a id={FormPanel.FORM_ELEMENT.COPY_IMAGE_NAME} class={cx(formStyle.copyLinks)}>(copy)</a>
                    <span class={cx(mdbStyles.badge, mdbStyles.badgePrimary, mdbStyles.badgePill)}>IMAGE</span>
                </li>
                <li class={cx(mdbStyles.listGroupItem, formStyle.formStnpad, 'd-flex', 'justify-content-between', 'align-items-center')}>
                    <span id={FormPanel.FORM_ELEMENT.TIME_SPENT} class={formStyle.longText}></span>
                    <span class={cx(mdbStyles.badge, mdbStyles.badgePrimary, mdbStyles.badgePill)}>TIME SPENT</span>
                </li>
                </ul>
            </div>
        )

    }

    /** Copy Image URL to clipboard */
    static copyToClipboard() {
        var range = document.createRange();
        range.selectNode(document.getElementById(FormPanel.FORM_ELEMENT.IMAGE_NAME)!);
        window.getSelection().removeAllRanges();
        window.getSelection().addRange(range);
        document.execCommand("copy");
        window.getSelection().removeAllRanges();
    }

    /**
     * Updates Task specific Information and its panel
     * @param data Text Needs to be inserted in this TextField
     */
    static updateTaskView(data:TaskAudio) {
        $('#'+FormPanel.FORM_ELEMENT.IMAGE_NAME).text(data._audioUrl)
        $('#'+FormPanel.FORM_ELEMENT.IMAGE_NAME).attr("title", data._audioUrl)

    }

    /**
     * Updates Batch Information and its panel
     * @param data Text Needs to be inserted in this TextField
     */
    static updateBatchView(data:TaskInfo) {
        $('#'+FormPanel.FORM_ELEMENT.TASK_ID).text(data.identifier);
    }

    /**
     * Update Time spent by the user in total
     * @param time duration in seconds
     */
    static updateTime(time:number) {
        this.time = time;
        $('#'+FormPanel.FORM_ELEMENT.TIME_SPENT).text(this.time)

    }

    /**
     * Used for Internal Handling to div visibility toggling
     * @param callback Not used
     */
    static getObservables(callback:Function) {
        fromEvent($('#'+FormPanel.FORM_ELEMENT.TASK_INFO_DIV), 'click')
            .subscribe(a => {
                $('#'+FormPanel.FORM_ELEMENT.POPBLOCK_DIV).toggle();
        })
        timer(1000, 1000).pipe(tap(a => {
            this.time += 1;
            $('#'+FormPanel.FORM_ELEMENT.TIME_SPENT).text(this.time)
        })).subscribe(a => {
            callback(Operation.TIME_UPDATE, this.time);
        })
    }
}


/**
 * @class
 * View Component for TextComments
 */
export class TextComment {
    /**
     * @static
     * Render View
     */
    static getUIComponent() {
        return (
            <input type="text" id={FormPanel.FORM_ELEMENT.COMMENT_DIV} class={cx('form-control', formStyle.mdForm, formStyle.formFix)} placeholder=" Post a Comment..." />
        )
    }

    /**
     * Updates Text InputField
     * @param data Text Needs to be inserted in this TextField
     */
    static updateView(data:any) {
        $('#'+FormPanel.FORM_ELEMENT.COMMENT_DIV).val(data)

    }

    /**
     * Calls the CallBack to inform View state change
     * @param callback Function of ViewState
     */
    static getObservables(callback : Function) {
        fromEvent($('#'+FormPanel.FORM_ELEMENT.COMMENT_DIV), 'change')
                .pipe(filter(() => FormPanel.formEnabled))
                .subscribe(a => {
                    let value = (a.target as HTMLInputElement).value
                    let key = (a.target! as HTMLElement).id
                    let ret :any = {}
                    ret[key] = value;
                    callback(Operation.COMMENT_CHANGE, ret)
                })
    }
}

export class SubmitComponent{
    static getUIComponent(){
        return (
            <div class="row" style="padding: 0px;text-align: right;">
                <div class={cx(formStyle.mdCheckbox)}>
                    <input id={FormPanel.FORM_ELEMENT.EXIT_TASKING} type="checkbox"/>
                    <label for={FormPanel.FORM_ELEMENT.EXIT_TASKING}>EOT</label>
                </div>
                <button id={FormPanel.FORM_ELEMENT.SUBMIT_BTN} type="submit" class={cx(mdbStyles.btn, formStyle.glow , 'btn-sm', 'btn-primary')} style="padding: 0px 15px; display: block; position: absolute; bottom: 3px; left: -9px; background: none !important; box-shadow: none;color: #4285f4 !important;font-weight: 700;text-transform: capitalize;font-size: 16px; margin: 0;">submit task</button>
            </div>
        );
    }

    /**
     * @static
     * ViewState callback
     */
    static _callback : Function;

    /**
     * @static
     * Subscription which can be revoked
     */
    static _subscription : Subscription;


    /**
     * Registers callback from ViewState
     * @param callback ViewState Callback
     */
    static getObservables(callback : Function) {
        this._callback = callback;
        fromEvent($('#'+FormPanel.FORM_ELEMENT.EXIT_TASKING), 'click')
        .subscribe(a => {
            callback(Operation.END_OF_TASK_REQUESTED)
        })
    }

    /**
     * @static
     * Enable/Disable view of this component based on taskinfo
     * @param data TaskInfo
     */
    static updateView(data: TaskInfo) {
        if (this._subscription) {
            this._subscription.unsubscribe()
        }
        document.getElementById(FormPanel.FORM_ELEMENT.SUBMIT_BTN)!.style.display = 'none'

        if (data.batchPosition === data.batchSize) {
            document.getElementById(FormPanel.FORM_ELEMENT.SUBMIT_BTN)!.style.display = 'block'
            this._subscription = fromEvent($('#'+FormPanel.FORM_ELEMENT.SUBMIT_BTN), 'click')
                .pipe(filter(() => FormPanel.formEnabled))
                .subscribe(a => {
                    //FormPanel.formEnabled = false;
                    this._callback(Operation.TASK_SUBMIT_REQUEST)
                })
        }
    }
}

/**
 * @class
 * Renders Select UI Component
 */
export class SelectableComment {
    /**
     * @static
     * ViewState callback
     */
    static _callback : Function;

    /**
     * @private @static
     * List of all MultiSelect Helper object which needs to be destroyed when switching
     */
    private static _multiSelectHelper = new Array<SlimSelect>();

    /**
     * @static
     * Renders View
     */
    static getUIComponent() {
        return (
            <div class="col-sm-10" style="padding-right: 0px;">
                <div class="row" id={FormPanel.FORM_ELEMENT.SELECTABLE_DIV}></div>
            </div>

        )
    }

    /**
     * @static
     * Creates the Popup block which contains segment information
     */
    static getPopupUIComponent () {
        return (
            <div id={FormPanel.FORM_ELEMENT.MULTI_SELECT_DIV} class={cx(formStyle.mselectBaseOpener)}>
            </div>
        );
    }

    /**
     * Registers callback from ViewState
     * @param callback ViewState Callback
     */
    static getObservables(callback : Function) {
        this._callback = callback;
    }

    /**
     * @static
     * Removes all MultiSelect Helper objects
     */
    static clearOptions() {
        this._multiSelectHelper.forEach(a => a.destroy());
        $('#'+FormPanel.FORM_ELEMENT.MULTI_SELECT_DIV).empty()
    }


    /**
     * @static
     * Helper to render multiple selectable input element in a single parent container
     * @param key Select options name
     * @param placeHolder String if nothing selected
     * @param defaultVal Default value selected
     * @param option Option values
     * @param select Selected Value
     * @param siblingCount Number of Select options present in the Same View Parent Component
     * @param multiEnable boolean flag for single/multi select
     */
    static generateOption(key: string, placeHolder: string, defaultVal : string, options: Config.Option[], select: string[]|null, siblingCount: number, multiEnable: boolean) {
        let percentage = Math.floor(12/siblingCount);
        let dataPlaceholder= placeHolder!==''? placeHolder :'Selected '+key+' displays here';
        if (multiEnable) {
            $('#'+FormPanel.FORM_ELEMENT.SELECTABLE_DIV).append(
                <div class={"col-sm-"+percentage}>
                    <span class={cx(formStyle.mselectOpener)} id={'SelectableDivPopup'+key}> {placeHolder}</span>
                </div>
            );


            $('#'+FormPanel.FORM_ELEMENT.MULTI_SELECT_DIV).append(
                <div id={'SelectableDiv'+key} class={"col-sm-16"} style="display:none">
                    <select id={'Selectable'+key} multiple="multiple" name={key} class={cx(formStyle.mdbSelect)}>
                    {options.map((item) => <option value={item.value}>{item.text}</option>)}
                    </select>
                </div>
            );

            let selectorSlim = this.createMultiSelectorHelper(key, dataPlaceholder)

            if (select && select.length !== 0) {
                selectorSlim.setSelected(select)
            } else if ( defaultVal !== "") {
                selectorSlim.setSelected(defaultVal)
            }
        } else {
            $('#'+FormPanel.FORM_ELEMENT.SELECTABLE_DIV).append(
                <div class={"col-sm-"+percentage}>
                    <select id={'Selectable'+key}  name={key} class={formStyle.mdbSelect}>
                        <option value="" selected disabled>{dataPlaceholder}</option>
                        {options.map((item) => <option value={item.value}>{item.text}</option>)}
                    </select>
                </div>
            );
            if (select && select.length !== 0) {
                $("#Selectable"+key).val(select)
            } else if ( defaultVal !== "") {
                $("#Selectable"+key).val(defaultVal)
            }
        }

        if (select && select.length !== 0) {
            $("#Selectable"+key).val(select)
        } else if ( defaultVal !== "") {
            $("#Selectable"+key).val(defaultVal)
        }

        fromEvent($('#Selectable'+key), 'change')
            .pipe(filter(() => FormPanel.formEnabled))
            .subscribe(a => {
                let key = (a.target! as HTMLInputElement).name
                let sel = (a.target! as HTMLSelectElement)
                let opts : any = [];
                for (var i=0, len=sel.options.length; i<len; i++) {
                    let opt : HTMLOptionElement = sel.options[i];

                    if ( opt.selected ) {
                        opts.push(opt.value);
                    }
                }
                let ret :any = {}
                ret[key] = opts;
                this._callback(Operation.COMMENT_CHANGE, ret)
            })
    }

    /**
     * @static
     * Bootstraps multi selector helper object
     * @param key Identifier for Multi Selector widget
     * @param dataPlaceholder Default string when nothing selected
     */
    static createMultiSelectorHelper(key: string, dataPlaceholder: string) {
        let selectorSlim = new SlimSelect({
            select: '#Selectable'+key,
            placeholder: dataPlaceholder,
            closeOnSelect: false
        })

        this._multiSelectHelper.push(selectorSlim);

        let openStatus = false;
        selectorSlim.onChange = (info: any) => {
            let length = (info as Array<Option>).length -1;
            if (length > 0) {
                let leastString = (info as Array<Option>).map(a => a.text).reduce(
                    (accum, val) => {if (accum === '') { return val; } else {return val.length > accum.length ? accum : val;}})
                $('#SelectableDivPopup'+key).text(leastString + '+'+length)
            } else if (length === 0 ) {
                $('#SelectableDivPopup'+key).text(info[0].text)
            } else {
                $('#SelectableDivPopup'+key).text(dataPlaceholder)
            }
        };

        selectorSlim.afterOpen = () => {openStatus = true}
        selectorSlim.afterClose = () => {openStatus = false; $('#SelectableDiv'+key).css('display', 'none');}

        $('#SelectableDivPopup'+key).click(
            () => {
                if (openStatus === true) return;
                $('#SelectableDiv'+key).css('display', 'block');
                selectorSlim.open();
            }
        )
        return selectorSlim;
    }
}

/**
 * @class
 * Previous Navigation Control
 */
export class PrevNavControl {
    /**
     * @static
     * Flag to disable/enable this control
     */
    private static controlEnable = true;
    /**
     * @static
     * Renders View
     */
    static getUIComponent() {
        return (
            <li class="page-item">
                <a class={cx('page-link', formStyle.paginationPageLinks, 'orange')}>
                    <i id={FormPanel.FORM_ELEMENT.PREV_BUTTON} class="material-icons">keyboard_arrow_left</i>
                </a>
            </li>
        )
    }

    /**
     * Registers callback for specific UI Element event
     * @param callback ViewState Callback
     */
    static getObservables(callback : Function) {
        fromEvent($('#prevNavButton'), 'click')
                .pipe(filter(() => this.controlEnable))
                .pipe(filter(() => FormPanel.formEnabled))
                .subscribe(a => {
                    console.log(a);
                    FormPanel.formEnabled = false;
                    callback(Operation.PREV_NAV_CLICK)
                })
    }

    /**
     * Update the buton based on position within the batch
     *@param data TaskInfo
    */
    static updateData(data : TaskInfo) {
        if (data.batchPosition === 1) {
            this.controlEnable = false;
        } else {
            this.controlEnable = true;
        }
    }
}

/**
 * @class
 * Next Navigation Control
 */
export class NextNavControl {
    /**
     * @static
     * Flag to disable/enable this control
     */
    private static controlEnable = true;
    /**

    /**
     * @static
     * Renders View
     */
    static getUIComponent() {
        return (
            <li class="page-item">
                <a class={cx('page-link', formStyle.paginationPageLinks, 'primary-color')} href="#">
                    <i id={FormPanel.FORM_ELEMENT.NEXT_BUTTON} class="material-icons">keyboard_arrow_right</i>
                </a>
            </li>
        )
    }

    /**
     * Registers callback for specific UI Element event
     * @param callback ViewState Callback
     */
    static getObservables(callback : Function) {
        fromEvent($('#nextNavButton'), 'click')
        .pipe(filter(() => this.controlEnable))
        .pipe(filter(() => FormPanel.formEnabled))
        .subscribe(a => {
            FormPanel.formEnabled = false;
            callback(Operation.NEXT_NAV_CLICK)
        })
    }

    /**
     * Update the buton based on position within the batch
     *@param data TaskInfo
    */
    static updateData(data : TaskInfo) {
        this.controlEnable = data.batchPosition !== data.batchSize;
    }
}

/**
 * @class
 * Navigation Information PlaceHolder Control
 */
export class NavInfo {
    /**
     * @static
     * Renders View
     */
    static getUIComponent() {
        return (
            <li id={FormPanel.FORM_ELEMENT.NAVINFO_DIV} class={cx('page-item', formStyle.lblfix)}>Fetching</li>
        )
    }

    /**
     * UI Updating function used by FormPanel
     * @param tinfo TaskInfo details
     */
    static updateData(tinfo : TaskInfo) {
        let str = "<input type ='text' style='width: 40%; height: 18px;text-align: center;' id='showText' value='"+tinfo.batchPosition+"' />" + " of " + tinfo.batchSize;
        $("#navInfo").html(str);
    }
}

export namespace FormPanel {
    /**
     * Set of Constants defining View IDs
     */
    export enum FORM_ELEMENT {
        TASK_INFO_DIV = 'task_information',
        TASK_ID = 'task_id',
        COMMENT_DIV = 'comment',
        POPBLOCK_DIV = 'popblock',
        PREV_BUTTON = 'prevNavButton',
        NAVINFO_DIV = 'navInfo',
        NEXT_BUTTON = 'nextNavButton',
        SELECTABLE_DIV = 'selectionDiv',
        IMAGE_NAME = 'task_image',
        TIME_SPENT = 'time_spent',
        SUBMIT_BTN = 'formpanel-submit',
        COPY_IMAGE_NAME = 'copyImgName',
        EXIT_TASKING = 'endOfTasking',
	    MULTI_SELECT_DIV = 'multissdiv'
    }

}
