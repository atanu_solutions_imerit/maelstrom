/**
 * @file This file house ViewState class viz, FormPanelState that contains all User
 * Text and Selectable Input for a single Task.
 * Second Class is a FormInput which contains logic to serialize and deserialize
 */
import { filter } from 'rxjs/operators';
import { Operation } from '../../services/messagebus/operation';
import { MessageBus } from '../../services/messagebus/messagebus';
import { Activity } from '../../services/messagebus/activity';
import { AudioElement } from '../../components/audio-element/audio-element';
import { TSMap } from 'typescript-map';
import { TaskAudio } from '../../services/messagebus/taskinfo';
import { FormInputSerializedData, MediaSerializedData, TimeLineSerializedData } from '../../services/messagebus/taskdata';

/**
 * @class
 * Class manages the Text and Selectable input for a single Task
 */
export class MediaState {
    /**
     * @static @private
     * FormInput handles which contains user input
     */
    private static _audioElement: AudioElement;

    private static _mState : MediaSerializedData;

    private static _timeline : TimeLineSerializedData;

    /**
     * End of Task status
     */
    static nextTaskRequest : boolean = false;


    /**
     * @static
     * Renders the View, registers its callback with View and Event Listener for global MessageBus
     * @param parentDiv HTMLDivElement on which all the View element will be rendered
     */
    static start(parentDiv: string) {
        this._audioElement = new AudioElement();
        this._mState = new MediaSerializedData();
        this._timeline = new TimeLineSerializedData();
        $('#'+parentDiv).append(this._audioElement.getView());
        this._audioElement.start(this.callbackHandler.bind(this));
        MessageBus.messageBusObservables
        .pipe(filter(a => a.action === Operation.ACTION_LOAD_TASK_REQUEST))
        .subscribe(a => {
            this._audioElement.updateClip((a.poststate as TaskAudio)._audioUrl);
        });
    }

    /**
     * @static
     * Function is the callback that needs to be registered with View.
     * @param type Set of Constant defining action taken by View
     * @param params Parameter required for that action
     */
    static callbackHandler(type: AudioElement.EVENT, ...params: Array<any>) {
        let prestate : any;
        let poststate : any;
        let obj : any;
        //console.log(type);
        switch (type) {
            case AudioElement.EVENT.PLAYBACK_POSITION:
                this._mState.playbackPosition = params[0];
                MessageBus.messageBusSubject.next(new Activity(Operation.PLAYBACK_CHANGE, undefined, undefined, this._mState));
                break;
            case AudioElement.EVENT.PLAYBACK_SPEED:
                this._mState.playbackSpeed = params[0];
                MessageBus.messageBusSubject.next(new Activity(Operation.PLAYBACK_CHANGE, undefined, undefined, this._mState));
                break;
            case AudioElement.EVENT.VOLUME:
                this._mState.volumeLevel = params[0];
                MessageBus.messageBusSubject.next(new Activity(Operation.PLAYBACK_CHANGE, undefined, undefined, this._mState));
                break;
            case AudioElement.EVENT.TIMELINEDATASTART:
                this._timeline.timedata.push(params[0]);
                MessageBus.messageBusSubject.next(new Activity(Operation.TIMELINEDATASTART, undefined, undefined, this._timeline));
                break;
            case AudioElement.EVENT.TIMELINEDATAEND:
                this._timeline.timedata.push(params[0]);
                MessageBus.messageBusSubject.next(new Activity(Operation.TIMELINEDATAEND, undefined, undefined,  this._timeline));
                break;

        }
    }
}

/**
 * @class FormInput
 * Text and Selectable Comment Holder
 */
export class FormInput {
    /**
     * TextComment for this Task
     */
    textComment: string = "";
    /**
     * Map of Selectable Item Name and Value(s) selected by user
     */
    predefinedComment = new TSMap<string, string[]>();
    /**
     * Image Partition details
     */
    imagePartition ?: Array<boolean>;
    /**
     * Time spent on this Task
     */
    timeSpent : number = 0;
    /**
     * Image Width
     */
    imageWidth : number = 0;
    /**
     * Image Height
     */
    imageHeight : number = 0;
    /**
     * Image URL
     */
    imageURL : string = '';
    /**
     * Image Category
     */
    imageCategory : string = '';


    /**
     * @property
     * Generates Serialized State of this object
     * @return FormInputSerializedData
     */
    get serializedData(): FormInputSerializedData {
        let tobj: any = {}
        this.predefinedComment.forEach((value, key) => {
            tobj[key!] = value;
        })
        let data: FormInputSerializedData = new FormInputSerializedData()
        data.textComment = this.textComment;
        data.predefinedComment = tobj;
        data.timeSpent = this.timeSpent;
        data.updated_on = Date.now()
        return data
    }

    /**
     * @property
     * Creates Object state from Serialized Version
     * @param object Serialized Data that will regenerated the Object state
     */
    set serializedData(object: FormInputSerializedData) {
        let data: any = object;
        this.textComment = object.textComment ? object.textComment : "";
        //this.imagePartition = object.imagePartition || [];
        this.timeSpent = object.timeSpent || 0;
        this.predefinedComment = new TSMap<string, string[]>();
        for (let key in object.predefinedComment) {
            this.predefinedComment.set(key, object.predefinedComment[key])
        }
    }

}
